package awsbatch

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"strings"

	"github.com/aws/aws-sdk-go/service/batch"
	"github.com/aws/aws-sdk-go/service/efs"
	"github.com/concourse/concourse/atc"
	"github.com/concourse/concourse/atc/exec"
)

func (c *executorAWSBatch) registerGetStepJobDefinition(batchSvc *batch.Batch, efsSvc *efs.EFS, jobid string, stepid string, plan atc.GetPlan) (*batch.RegisterJobDefinitionOutput, []int, []string, []string, error) {
	planSource, err := json.Marshal(map[string]interface{}{"source": plan.Source})
	if err != nil {
		return nil, nil, nil, nil, fmt.Errorf("marshal plan source: %w", err)
	}
	var canarys []int

	if plan.Type == "registry-image" {
		return nil, nil, nil, nil, nil
	}

	baseImage := atc.GetCheckStepBaseImage(plan.Type)
	canary := rand.Int() % 1000
	canarys = append(canarys, canary)

	dir := fmt.Sprintf("/tmp/build/%d/%s", canary, plan.Name)

	scripts := []string{
		fmt.Sprintf("echo '%s' > /tmp/in-request", string(planSource[:])),
		fmt.Sprintf("/opt/resource/in %s < /tmp/in-request > /tmp/in-result", dir),
		fmt.Sprintf("echo 'BGN-%d'", canary),
		"cat /tmp/in-result",
		fmt.Sprintf("echo 'END-%d'", canary),
	}
	cmd := strings.Join(scripts, " && ")

	fmt.Println("tugas-akhir-cmd", cmd)

	outputs := []atc.TaskOutputConfig{{Name: plan.Name, Path: dir}}
	volumes, mountPoints, fileSystemIds, mountTargetIds, err := c.getStepVolumesAndMountPoints(efsSvc, jobid, stepid, []atc.TaskInputConfig{}, outputs)
	if err != nil {
		return nil, nil, nil, nil, fmt.Errorf("get step volumes and mount points: %w", err)
	}

	registerJobDefinitionOutput, err := c.registerJobDefinition(batchSvc, stepid, baseImage, cmd, volumes, mountPoints, nil)

	if err != nil {
		return nil, nil, nil, nil, fmt.Errorf("register job definition: %w", err)
	}

	return registerJobDefinitionOutput, canarys, fileSystemIds, mountTargetIds, nil
}

func (c *executorAWSBatch) transferGetBuildLog(
	plan atc.GetPlan,
	delegate exec.BuildStepDelegate,
	procOuts [][]byte,
	procErrs [][]byte,
) ([][]byte, [][]byte, error) {
	if plan.Type == "registry-image" {
		return procOuts, procErrs, nil
	} 
	delegate.Stdout().Write(procOuts[0])
	delegate.Stderr().Write(procErrs[0])

	return procOuts[1:], procErrs[1:], nil
}
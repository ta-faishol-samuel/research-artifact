package awsbatch

import (
	"context"
	"fmt"

	"code.cloudfoundry.org/lager"
	"code.cloudfoundry.org/lager/lagerctx"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/batch"
	"github.com/aws/aws-sdk-go/service/cloudwatchlogs"
	"github.com/aws/aws-sdk-go/service/efs"
	"github.com/concourse/concourse/atc"
	"github.com/concourse/concourse/atc/exec"
	"github.com/google/uuid"
)

func (c *executorAWSBatch) ExecuteDoStep(ctx context.Context, plan atc.DoPlan, planDelegate []exec.BuildStepDelegate) (bool, error) {
	jobid := uuid.NewString()
	region, jobQueueName := c.getRegionAndJobQueueNameAlsoSetAccessKeyEnv()
	c.region = region
	c.jobQueueName = jobQueueName

	fmt.Println("tugas-akhir masuk execute do step")

	session, err := session.NewSessionWithOptions(session.Options{
		Config: aws.Config{Region: aws.String(c.region)},
	})
	if err != nil {
		return false, fmt.Errorf("create session: %w", err)
	}

	batchSvc := batch.New(session)
	efsSvc := efs.New(session)

	mainProcErrs := [][]byte{}
	mainProcOuts := [][]byte{}

	var successful bool

	jobDefinitionArns, canarys, fileSystemIds, mountTargetIds, err := c.createDoStepJobDefinition(batchSvc, efsSvc, jobid, plan)
	if err != nil {
		return false, fmt.Errorf("create do step job definition: %w", err)
	}

	fmt.Println("tugas-akhir selesai bikin job def do step")
	
	for _, jobDefinitionArn := range jobDefinitionArns {
		submitJobOutput, err := submitJob(batchSvc, jobDefinitionArn, jobid, c.jobQueueName)
		if err != nil {
			return false, fmt.Errorf("submit job: %w", err)
		}

		jobId := submitJobOutput.JobId

		cloudwatchlogsSvc := cloudwatchlogs.New(session)

		procOuts, procErrs, getSuccessful, err := c.waitUntilJobFinishedAndGetAllLogs(batchSvc, cloudwatchlogsSvc, *jobId, canarys)
		if err != nil {
			return false, err
		}

		if !getSuccessful {
			successful = false
		}

		for _, procErr := range procErrs {
			fmt.Println("tugas-akhir", procErr)
		}

		for _, procOut := range procOuts {
			fmt.Println("tugas-akhir", procOut)
		}

		mainProcErrs = append(mainProcErrs, procErrs...)
		mainProcOuts = append(mainProcOuts, procOuts...)
	}

	fmt.Println("tugas-akhir selesai submit job")

	for _, jobDefinitionArn := range jobDefinitionArns {
		err = deregisterJobDefinition(batchSvc, jobDefinitionArn)
		if err != nil {
			return false, fmt.Errorf("deregister job definition: %w", err)
		}
	}

	fmt.Println("tugas-akhir selesai deregister job def")

	for _, mountTargetId := range mountTargetIds {
		err = deleteMountTarget(efsSvc, mountTargetId)
		if err != nil {
			return false, fmt.Errorf("delete mount target: %w", err)
		}
	}

	fmt.Println("tugas-akhir selesai delete mount target")

	for _, fileSystemId := range fileSystemIds {
		err = deleteFileSystem(efsSvc, fileSystemId)
		if err != nil {
			return false, fmt.Errorf("delete file system: %w", err)
		}
	}

	fmt.Println("tugas-akhir selesai delete file system")

	c.transferDoBuildLog(ctx, plan, planDelegate, mainProcOuts, mainProcErrs)

	return successful, nil
}

func (c *executorAWSBatch) createDoStepJobDefinition(batchSvc *batch.Batch, efsSvc *efs.EFS, jobid string, plan atc.DoPlan) ([]string, []int, []string, []string, error) {
	var canarys []int
	resources := make(map[string]atc.GetPlan)

	var jobDefinitionArns []string
	var fileSystemIds []string
	var mountTargetIds []string

	for _, innerPlan := range plan {
		if innerPlan.Get != nil {
			resources[innerPlan.Get.Name] = *innerPlan.Get
		}

		jobDefinitionOutput, canary, newFileSystemIds, newMountTargetIds, err := c.buildConvertStep(batchSvc, efsSvc, jobid, innerPlan, resources)
		if err != nil {
			return nil, []int{-1}, nil, nil, fmt.Errorf("register job definition: %w", err)
		}

		fileSystemIds = append(fileSystemIds, newFileSystemIds...)
		mountTargetIds = append(mountTargetIds, newMountTargetIds...)

		if jobDefinitionOutput == nil {
			continue
		}

		jobDefinitionArn := *jobDefinitionOutput.JobDefinitionArn
		fmt.Println("tugas-akhir-jobDefinitionArn: ", jobDefinitionArn)
		jobDefinitionArns = append(jobDefinitionArns, jobDefinitionArn)
		canarys = append(canarys, canary...)
	}
	
	return jobDefinitionArns, canarys, fileSystemIds, mountTargetIds, nil
}

func (c *executorAWSBatch) buildConvertStep(batchSvc *batch.Batch, efsSvc *efs.EFS, jobid string, plan atc.Plan, resources map[string]atc.GetPlan) (*batch.RegisterJobDefinitionOutput, []int, []string, []string, error) {
	if plan.Get != nil {
		resources[string(plan.ID)] = *plan.Get
		return c.registerGetStepJobDefinition(batchSvc, efsSvc, jobid, string(plan.ID), *plan.Get)
	} 
	if plan.Task != nil {
		return c.registerTaskStepJobDefinition(batchSvc, efsSvc, jobid, string(plan.ID), *plan.Task, resources)
	} 
	return nil, nil, nil, nil, fmt.Errorf("step is not supported")
}

func (c *executorAWSBatch) transferDoBuildLog(
	ctx context.Context,
	plan atc.DoPlan,
	planDelegate []exec.BuildStepDelegate,
	procOuts [][]byte,
	procErrs [][]byte,
) error {
	var err error
	for i, innerPlan := range plan {
		delegate := planDelegate[i]

		logger := lagerctx.FromContext(ctx)
		logger = logger.Session("do-inner-step", lager.Data{
			"plan-id": innerPlan.ID,
		})
		delegate.Initializing(logger)
		delegate.Starting(logger)

		procOuts, procErrs, err = c.extractPlanBuildLog(innerPlan, delegate, procOuts, procErrs)
		if err != nil {
			return err
		}
		delegate.Finished(logger, true)
	}
	return nil
}

func (c *executorAWSBatch) extractPlanBuildLog(
	plan atc.Plan,
	delegate exec.BuildStepDelegate,
	procOuts [][]byte,
	procErrs [][]byte,
) ([][]byte, [][]byte, error) {
	if plan.Get != nil {
		return c.transferGetBuildLog(*plan.Get, delegate, procOuts, procErrs)
	}
	if plan.Task != nil {
		return c.transferTaskBuildLog(*plan.Task, delegate, procOuts, procErrs)
	}
	return procOuts, procErrs, fmt.Errorf("step is not supported")
}
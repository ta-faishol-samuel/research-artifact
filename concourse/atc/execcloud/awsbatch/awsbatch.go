package awsbatch

import (
	"bytes"
	"encoding/json"
	"fmt"
	"math"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/batch"
	"github.com/aws/aws-sdk-go/service/cloudwatchlogs"
	"github.com/aws/aws-sdk-go/service/efs"
	"github.com/concourse/concourse/atc"
	"github.com/concourse/concourse/atc/exec"
	"github.com/concourse/flag"
)

type executorAWSBatch struct {
	options			flag.File
	region			string
	jobQueueName	string
	subnetId		string
	executionRoleArn string
}

func NewExecutorAWSBatch(
	cred flag.File,
) exec.CloudExecutor {
	return &executorAWSBatch{
		options: cred,
		region: "",
		jobQueueName: "",
		subnetId: "",
		executionRoleArn: "",
	}
}

func (c *executorAWSBatch) getRegionAndJobQueueNameAlsoSetAccessKeyEnv() (string, string) {
	if len(c.region) == 0 || len(c.jobQueueName) == 0 || len(os.Getenv("AWS_ACCESS_KEY_ID")) == 0 || len(os.Getenv("AWS_SECRET_ACCESS_KEY")) == 0 {
		credContent, _ := os.ReadFile(c.options.Path())
		var data map[string]string
		err := json.Unmarshal(credContent, &data)
		if err != nil {
			return "", ""
		}
		c.region = data["region"]
		c.jobQueueName = data["job_queue_name"]
		c.subnetId = data["subnet_id"]
		c.executionRoleArn = data["execution_role_arn"]

		os.Setenv("AWS_ACCESS_KEY_ID", data["access_key_id"])
		os.Setenv("AWS_SECRET_ACCESS_KEY", data["secret_access_key"])
	}

	return c.region, c.jobQueueName
}

func (c *executorAWSBatch) registerJobDefinition(svc *batch.Batch, stepid string, image string, cmd string, volumes []*batch.Volume, mountPoints []*batch.MountPoint, environments []*batch.KeyValuePair) (*batch.RegisterJobDefinitionOutput, error) {
	registerJobDefinintionInput := &batch.RegisterJobDefinitionInput{
		ContainerProperties: &batch.ContainerProperties{
			Command: []*string{
				aws.String("/bin/sh"),
				aws.String("-c"),
				aws.String(cmd),
			},
			ExecutionRoleArn: aws.String(c.executionRoleArn),
			Image: aws.String(image),
			ResourceRequirements: []*batch.ResourceRequirement{
				{
					Type: aws.String("VCPU"),
					Value: aws.String("1"),
				},
				{
					Type: aws.String("MEMORY"),
					Value: aws.String("4096"),
				},
			},
			NetworkConfiguration: &batch.NetworkConfiguration{
				AssignPublicIp: aws.String("ENABLED"),
			},
			Volumes: volumes,
			MountPoints: mountPoints,
			Environment: environments,
		},
		JobDefinitionName: aws.String(stepid),
		Type: aws.String("container"),
		PlatformCapabilities: []*string{
			aws.String("FARGATE"),
		},
	}

	for {
		registerJobDefinitionOutput, err := svc.RegisterJobDefinition(registerJobDefinintionInput)
		if err != nil {
			if aerr, ok := err.(awserr.Error); ok {
				if aerr.Code() == "ThrottlingException" {
					time.Sleep(15 * time.Second)
					continue
				}
			}
			return nil, fmt.Errorf("register job definition: %w", err)
		}
		return registerJobDefinitionOutput, nil
	}
}

func submitJob(svc *batch.Batch, jobDefinitionArn string, stepid string, jobQueueName string) (*batch.SubmitJobOutput, error) {

	submitJobInput := &batch.SubmitJobInput{
		JobDefinition: aws.String(jobDefinitionArn),
		JobName: aws.String(stepid),
		JobQueue: aws.String(jobQueueName),
	}

	for {
		submitJobOutput, err := svc.SubmitJob(submitJobInput)
		if err != nil {
			if aerr, ok := err.(awserr.Error); ok {
				if aerr.Code() == "ThrottlingException" {
					time.Sleep(15 * time.Second)
					continue
				}
			}
			return nil, fmt.Errorf("submit job: %w", err)
		}
		return submitJobOutput, nil
	}
}

func getLogFromAWS(logStreamName string, svc *cloudwatchlogs.CloudWatchLogs) ([]string, error) {
	getLogEventsInput := &cloudwatchlogs.GetLogEventsInput{
		LogGroupName: aws.String("/aws/batch/job"),
		LogStreamName: aws.String(logStreamName),
	}

	var getLogEventsOutput *cloudwatchlogs.GetLogEventsOutput
	var err error
	
	for {
		getLogEventsOutput, err = svc.GetLogEvents(getLogEventsInput)
		if err != nil {
			if aerr, ok := err.(awserr.Error); ok {
				if aerr.Code() == "ThrottlingException" {
					time.Sleep(15 * time.Second)
					continue
				}
			}

			return nil, fmt.Errorf("get log events: %w", err)
		}
		break
	}

	var log []string
	for _, event := range getLogEventsOutput.Events {
		log = append(log, string(*event.Message))
	}

	return log, nil
}

func (c *executorAWSBatch) waitUntilJobFinishedAndGetAllLogs(batchSvc *batch.Batch, cloudwatchlogsSvc *cloudwatchlogs.CloudWatchLogs, jobID string, canarys []int) ([][]byte, [][]byte, bool, error) {
	describeJobsInput := &batch.DescribeJobsInput{
		Jobs: []*string{
			aws.String(jobID),
		},
	}

	for {
		describeJobsOutput, err := batchSvc.DescribeJobs(describeJobsInput)
		if err != nil {
			// Check if error is throttling error then sleep and retry
			if aerr, ok := err.(awserr.Error); ok {
				if aerr.Code() == "ThrottlingException" {
					time.Sleep(15 * time.Second)
					continue
				}
			}
			return nil, nil, false, fmt.Errorf("wait job until finished: %w", err)
		}

		if *describeJobsOutput.Jobs[0].Status == "SUCCEEDED" || *describeJobsOutput.Jobs[0].Status == "FAILED" {
			logStreamName := *describeJobsOutput.Jobs[0].Container.LogStreamName
			
			log, err := getLogFromAWS(logStreamName, cloudwatchlogsSvc)
			if err != nil {
				return nil, nil, false, err
			}

			resStdout, resStderr := c.parseLogOutput(log, canarys)

			if *describeJobsOutput.Jobs[0].Status == "FAILED" {
				return resStdout, resStderr, false, nil
			}

			return resStdout, resStderr, true, nil
		}

		time.Sleep(15 * time.Second)
	}
}

func (c *executorAWSBatch) parseLogOutput(logStrings []string, canarys [] int) ([][]byte, [][]byte) {
	var resStdout [][]byte
	var resStderr [][]byte

	log := []byte(strings.Join(logStrings, "\n"))
	
	posStr := 0

	for _, canary := range canarys {
		var (
			nowOut []byte
			nowErr []byte
		)

		if posStr >= len(log) {
			break
		}
		log = log[posStr:]

		endErr := fmt.Sprintf("BGN-%d", canary)
		posEnd := bytes.Index(log, []byte(endErr))

		if posEnd == -1 {
			// Stdout is empty
			posEnd = len(log)

			nowErr = log
			nowOut = []byte{}
		} else {
			// Stdout is not empty
			nowErr = log[:int(math.Max(float64(posEnd - 1), 0))]
			log = log[posEnd+len(endErr):]
			
			// Get stdout
			signEnd := fmt.Sprintf("END-%d", canary)
			posEnd = bytes.Index(log, []byte(signEnd))

			nowOut = log[:posEnd - 1]
		}

		resStderr = append(resStderr, nowErr)
		resStdout = append(resStdout, nowOut)
		log = log[posEnd:]

		signEnd := fmt.Sprintf("END-%d", canary)
		posStr = posEnd + len(signEnd)
	}

	return resStdout, resStderr
}

func (c *executorAWSBatch) createVolume(efsSvc *efs.EFS, creationToken string) (string, string, error) {
	describeFileSystemsInput := &efs.DescribeFileSystemsInput{
		CreationToken: aws.String(creationToken),
	}

	var describeFileSystemsOutput *efs.DescribeFileSystemsOutput
	var describeMountTargetsOutput *efs.DescribeMountTargetsOutput
	var err error

	for {
		describeFileSystemsOutput, err = efsSvc.DescribeFileSystems(describeFileSystemsInput)
		if err != nil {
			if aerr, ok := err.(awserr.Error); ok {
				if aerr.Code() == "ThrottlingException" {
					time.Sleep(15 * time.Second)
					continue
				}
			}

			return "", "", fmt.Errorf("describe file systems: %w", err)
		}
		break
	}

	if len(describeFileSystemsOutput.FileSystems) == 0 {
		createFileSystemInput := &efs.CreateFileSystemInput{
			Backup: aws.Bool(false),
			CreationToken: aws.String(creationToken),
			Encrypted: aws.Bool(false),
			PerformanceMode: aws.String("generalPurpose"),
		}
	
		var createFileSystemOutput *efs.FileSystemDescription
		for {
			createFileSystemOutput, err = efsSvc.CreateFileSystem(createFileSystemInput)
			if err != nil {
				if aerr, ok := err.(awserr.Error); ok {
					if aerr.Code() == "ThrottlingException" {
						time.Sleep(15 * time.Second)
						continue
					}
				}

				return "", "", fmt.Errorf("create file system: %w", err)
			}
			break
		}
	
		fileSystemId := *createFileSystemOutput.FileSystemId
		createMountTargetInput := &efs.CreateMountTargetInput{
			FileSystemId: aws.String(fileSystemId),
			SubnetId: aws.String(c.subnetId),
		}

		for {
			describeFileSystemsInput := &efs.DescribeFileSystemsInput{
				CreationToken: aws.String(creationToken),
			}

			describeFileSystemsOutput, err := efsSvc.DescribeFileSystems(describeFileSystemsInput)
			if err != nil {
				if aerr, ok := err.(awserr.Error); ok {
					if aerr.Code() == "ThrottlingException" {
						time.Sleep(15 * time.Second)
						continue
					}
				}
				return "", "", fmt.Errorf("describe file systems: %w", err)
			}

			lifeCycleState := *describeFileSystemsOutput.FileSystems[0].LifeCycleState

			if lifeCycleState == "available" {
				break
			}

			time.Sleep(15 * time.Second)
		}

		var createMountTargetOutput *efs.MountTargetDescription
		var err error

		for {
			createMountTargetOutput, err = efsSvc.CreateMountTarget(createMountTargetInput)
			if err != nil {
				if aerr, ok := err.(awserr.Error); ok {
					if aerr.Code() == "ThrottlingException" {
						time.Sleep(15 * time.Second)
						continue
					}
				}
	
				return "", "", fmt.Errorf("create mount target: %w", err)
			}
			break
		}

		mountTargetId := *createMountTargetOutput.MountTargetId

		for {
			describeMountTargetsInput := &efs.DescribeMountTargetsInput{
				FileSystemId: aws.String(fileSystemId),
			}

			describeMountTargetsOutput, err := efsSvc.DescribeMountTargets(describeMountTargetsInput)
			if err != nil {
				if aerr, ok := err.(awserr.Error); ok {
					if aerr.Code() == "ThrottlingException" {
						time.Sleep(15 * time.Second)
						continue
					}
				}
				return "", "", fmt.Errorf("describe mount targets: %w", err)
			}

			lifeCycleState := *describeMountTargetsOutput.MountTargets[0].LifeCycleState

			if lifeCycleState == "available" {
				break
			}

			time.Sleep(15 * time.Second)
		}

		return fileSystemId, mountTargetId, nil
	}

	fileSystemId := *describeFileSystemsOutput.FileSystems[0].FileSystemId

	describeMountTargetsInput := &efs.DescribeMountTargetsInput{
		FileSystemId: aws.String(fileSystemId),
	}

	for {
		describeMountTargetsOutput, err = efsSvc.DescribeMountTargets(describeMountTargetsInput)
		if err != nil {
			if aerr, ok := err.(awserr.Error); ok {
				if aerr.Code() == "ThrottlingException" {
					time.Sleep(15 * time.Second)
					continue
				}
			}

			return "", "", fmt.Errorf("describe mount targets: %w", err)
		}
		break
	}

	mountTargetId := *describeMountTargetsOutput.MountTargets[0].MountTargetId

	return fileSystemId, mountTargetId, nil
}

func resolvePath(workingDir string, path string) string {
	if filepath.IsAbs(path) {
		return path
	}
	return filepath.Join(workingDir, path)
}

func artifactPath(workingDir string, name string, path string) string {
	subdir := path
	if path == "" {
		subdir = name
	}

	return resolvePath(workingDir, subdir)
}

func getInitialWorkdir(stepid string) string {
	return filepath.Join("/tmp/build", stepid)
}

func (c *executorAWSBatch) getStepVolumesAndMountPoints(efsSvc *efs.EFS, jobid string, stepid string, inputs []atc.TaskInputConfig, outputs []atc.TaskOutputConfig) ([]*batch.Volume, []*batch.MountPoint, []string, []string, error) {
	workdir := getInitialWorkdir(stepid)

	volumes := []*batch.Volume{}
	mountPoints := []*batch.MountPoint{}
	
	fileSystemIds := []string{}
	mountTargetIds := []string{}

	for _, output := range outputs {
		fmt.Println("tugas-akhir Creating volume for output", jobid, output.Name)
		fileSystemId, mountTargetId, err := c.createVolume(efsSvc, jobid+"-"+output.Name)
		if err != nil {
			return nil, nil, nil, nil, fmt.Errorf("create volume: %w", err)
		}

		fileSystemIds = append(fileSystemIds, fileSystemId)
		mountTargetIds = append(mountTargetIds, mountTargetId)

		currVol := &batch.Volume{
			EfsVolumeConfiguration: &batch.EFSVolumeConfiguration{
				FileSystemId: aws.String(fileSystemId),
				RootDirectory: aws.String("/"),
			},
			Name: aws.String(output.Name),
		}

		volumes = append(volumes, currVol)

		currMountPoint := &batch.MountPoint{
			ContainerPath: aws.String(artifactPath(workdir, output.Name, output.Path)),
			ReadOnly: aws.Bool(false),
			SourceVolume: aws.String(output.Name),
		}

		mountPoints = append(mountPoints, currMountPoint)
	}

	for _, input := range inputs {
		fmt.Println("tugas-akhir Creating volume for input", jobid, input.Name)
		describeFileSystemsInput := &efs.DescribeFileSystemsInput{
			CreationToken: aws.String(jobid+"-"+input.Name),
		}

		var describeFileSystemsOutput *efs.DescribeFileSystemsOutput
		var err error
		
		for {
			describeFileSystemsOutput, err = efsSvc.DescribeFileSystems(describeFileSystemsInput)
			if err != nil {
				if aerr, ok := err.(awserr.Error); ok {
					if aerr.Code() == "ThrottlingException" {
						time.Sleep(15 * time.Second)
						continue
					}
				}
				return nil, nil, nil, nil, fmt.Errorf("describe file systems: %w", err)
			}
			break
		}

		if len(describeFileSystemsOutput.FileSystems) == 0 {
			return nil, nil, nil, nil, fmt.Errorf("file system not found")
		}

		fileSystemId := *describeFileSystemsOutput.FileSystems[0].FileSystemId

		currVol := &batch.Volume{
			EfsVolumeConfiguration: &batch.EFSVolumeConfiguration{
				FileSystemId: aws.String(fileSystemId),
				RootDirectory: aws.String("/"),
			},
			Name: aws.String(input.Name),
		}

		volumes = append(volumes, currVol)
		
		currMountPoint := &batch.MountPoint{
			ContainerPath: aws.String(artifactPath(workdir, input.Name, input.Path)),
			ReadOnly: aws.Bool(false),
			SourceVolume: aws.String(input.Name),
		}

		mountPoints = append(mountPoints, currMountPoint)
	}

	return volumes, mountPoints, fileSystemIds, mountTargetIds, nil
}

func deregisterJobDefinition(batchSvc *batch.Batch, jobDefinitionArn string) error {
	deregisterJobDefinitionInput := &batch.DeregisterJobDefinitionInput{
		JobDefinition: aws.String(jobDefinitionArn),
	}

	for {
		_, err := batchSvc.DeregisterJobDefinition(deregisterJobDefinitionInput)
		if err != nil {
			if aerr, ok := err.(awserr.Error); ok {
				if aerr.Code() == "ThrottlingException" {
					time.Sleep(15 * time.Second)
					continue
				}
			}

			return fmt.Errorf("deregister job definition: %w", err)
		}
		break
	}

	return nil
}

func deleteFileSystem(efsSvc *efs.EFS, fileSystemId string) error {
	deleteFileSystemInput := &efs.DeleteFileSystemInput{
		FileSystemId: aws.String(fileSystemId),
	}

	for {
		_, err := efsSvc.DeleteFileSystem(deleteFileSystemInput)
		if err != nil {
			if aerr, ok := err.(awserr.Error); ok {
				if aerr.Code() == "ThrottlingException" {
					time.Sleep(15 * time.Second)
					continue
				}
			}

			return fmt.Errorf("delete file system: %w", err)
		}
		break
	}

	return nil
}

func deleteMountTarget(efsSvc *efs.EFS, mountTargetId string) error {
	deleteMountTargetInput := &efs.DeleteMountTargetInput{
		MountTargetId: aws.String(mountTargetId),
	}

	for {
		_, err := efsSvc.DeleteMountTarget(deleteMountTargetInput)
		if err != nil {
			if aerr, ok := err.(awserr.Error); ok {
				if aerr.Code() == "ThrottlingException" {
					time.Sleep(15 * time.Second)
					continue
				}
			}

			return fmt.Errorf("delete mount target: %w", err)
		}
		break
	}

	// check if mount target is deleted
	describeMountTargetsInput := &efs.DescribeMountTargetsInput{
		MountTargetId: aws.String(mountTargetId),
	}

	for {
		fmt.Println("tugas-akhir Checking mount target status", mountTargetId)
		describeMountTargetsOutput, err := efsSvc.DescribeMountTargets(describeMountTargetsInput)
		if err != nil {
			if aerr, ok := err.(awserr.Error); ok {
				if aerr.Code() == "ThrottlingException" {
					fmt.Println("tugas-akhir ThrottlingException")
					time.Sleep(15 * time.Second)
					continue
				}

				if aerr.Code() == "MountTargetNotFound" {
					fmt.Println("tugas-akhir Mount target deleted")
					break
				}
			}

			return fmt.Errorf("describe mount targets: %w", err)
		}

		fmt.Println("tugas-akhir Mount target status", *describeMountTargetsOutput)

		if len(describeMountTargetsOutput.MountTargets) == 0 {
			break
		}

		time.Sleep(1 * time.Second)
	}


	return nil
}
package awsbatch

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"math/rand"
	"strings"

	"github.com/concourse/concourse/atc"
	"github.com/google/uuid"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/batch"
	"github.com/aws/aws-sdk-go/service/cloudwatchlogs"
)

func (c *executorAWSBatch) ExecuteCheckStep(ctx context.Context, plan atc.CheckPlan, stderr io.Writer) ([]atc.Version, error) {
	jobid := uuid.NewString()
	region, jobQueueName := c.getRegionAndJobQueueNameAlsoSetAccessKeyEnv()
	c.region = region
	c.jobQueueName = jobQueueName

	session, err := session.NewSessionWithOptions(session.Options{
		Config: aws.Config{Region: aws.String(c.region)},
	})
	if err != nil {
		return nil, fmt.Errorf("create session: %w", err)
	}

	batchSvc := batch.New(session)
	jobDefinitionOutput, canary, err := c.createCheckStepJobDefinition(batchSvc, jobid, plan)
	if err != nil {
		return nil, fmt.Errorf("create check step job definition: %w", err)
	}

	jobDefinitionArn := jobDefinitionOutput.JobDefinitionArn

	submitJobOutput, err := submitJob(batchSvc, *jobDefinitionArn, jobid, c.jobQueueName)
	if err != nil {
		return nil, fmt.Errorf("submit job: %w", err)
	}

	jobId := submitJobOutput.JobId

	cloudwatchlogsSvc := cloudwatchlogs.New(session)

	procOuts, procErrs, _, err := c.waitUntilJobFinishedAndGetAllLogs(batchSvc, cloudwatchlogsSvc, *jobId, []int{canary})
	if err != nil {
		return nil, err
	}

	deregisterJobDefinition(batchSvc, *jobDefinitionArn)

	for _, procErr := range procErrs {
		stderr.Write(procErr)
	}

	var versions []atc.Version
	err = json.Unmarshal(procOuts[0], &versions)
	if err != nil {
		return nil, fmt.Errorf("unmarshal process output: %w", err)
	}

	return versions, nil
}

func (c *executorAWSBatch) createCheckStepJobDefinition(batchSvc *batch.Batch, id string, plan atc.CheckPlan) (*batch.RegisterJobDefinitionOutput, int, error) {
	planSource, err := json.Marshal(map[string]interface{}{"source": plan.Source})
	if err != nil {
		return nil, -1, fmt.Errorf("marshal check plan source: %w", err)
	}

	baseImage := atc.GetCheckStepBaseImage(plan.Type)
	canary := rand.Int() % 1000
	scripts := []string{
		fmt.Sprintf("echo '%s' > /tmp/check-request", string(planSource[:])),
		"/opt/resource/check < /tmp/check-request > /tmp/check-result",
		fmt.Sprintf("echo 'BGN-%d'", canary),
		"cat /tmp/check-result",
		fmt.Sprintf("echo 'END-%d'", canary),
	}
	cmd := strings.Join(scripts, " && ")

	registerJobDefinitionOutput, err := c.registerJobDefinition(batchSvc, id, baseImage, cmd, nil, nil, nil)

	if err != nil {
		return nil, -1, fmt.Errorf("register job definition: %w", err)
	}

	return registerJobDefinitionOutput, canary, nil
}
package awsbatch

import (
	"fmt"
	"math/rand"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/aws/aws-sdk-go/service/batch"
	"github.com/aws/aws-sdk-go/service/efs"
	"github.com/concourse/concourse/atc"
	"github.com/concourse/concourse/atc/exec"
)

func parseImageResourceSource(imageConfig atc.Source) string {
	imageName := imageConfig["repository"].(string)
	imageTag, exist := imageConfig["tag"].(string)
	if exist {
		imageName = fmt.Sprintf("%s:%s", imageName, imageTag)
	}
	return imageName
}

func (c *executorAWSBatch) registerTaskStepJobDefinition(batchSvc *batch.Batch, efsSvc *efs.EFS, jobid string, stepid string, plan atc.TaskPlan, resources map[string]atc.GetPlan) (*batch.RegisterJobDefinitionOutput, []int, []string, []string, error) {
	var canarys []int
	canary := rand.Int() % 1000
	canarys = append(canarys, canary)

	fmt.Println("tugas-akhir-register-task-step-job-definition", plan)

	var imageConfig atc.Source
	if len(plan.ImageArtifactName) != 0 {
		fmt.Println("tugas-akhir-image-artifact-name", plan.ImageArtifactName)
		getPlan := resources[plan.ImageArtifactName]
		imageConfig = getPlan.Source
	} else {
		fmt.Println("tugas-akhir-image-resource-source", plan.Config.ImageResource.Source)
		imageConfig = plan.Config.ImageResource.Source
	}

	fmt.Println("tugas-akhir-image-config", imageConfig)

	imageName := parseImageResourceSource(imageConfig)

	environments := []*batch.KeyValuePair{}
	for k, v := range plan.Config.Params {
		environments = append(environments, &batch.KeyValuePair{
			Name:  &k,
			Value: &v,
		})
	}

	buildWorkDir := filepath.Join("/tmp/build", strconv.Itoa(canary))

	inputs := []atc.TaskInputConfig{}
	for _, input := range plan.Config.Inputs {
		inputs = append(inputs, atc.TaskInputConfig{
			Name:     input.Name,
			Path:     artifactPath(buildWorkDir, input.Name, input.Path),
			Optional: input.Optional,
		})

		fmt.Println("tugas-akhir-inputs", jobid, input.Name)
	}

	outputs := []atc.TaskOutputConfig{}
	for _, output := range plan.Config.Outputs {
		outputs = append(outputs, atc.TaskOutputConfig{
			Name:     output.Name,
			Path:     artifactPath(buildWorkDir, output.Name, output.Path),
		})

		fmt.Println("tugas-akhir-outputs", jobid, output.Name)
	}
	
	volumes, mountPoints, fileSystemIds, mountTargetIds, err := c.getStepVolumesAndMountPoints(efsSvc, jobid, stepid, inputs, outputs)
	if err != nil {
		return nil, nil, nil, nil, fmt.Errorf("get step volumes and mount points: %w", err)
	}

	path := plan.Config.Run.Path
	args := plan.Config.Run.Args

	workdir := resolvePath(buildWorkDir, plan.Config.Run.Dir)
	cdCmd := fmt.Sprintf("cd %s &&", workdir)
	
	scripts := strings.Join([]string{cdCmd, path, strings.Join(args, " ")}, "\n")
	strings.Join([]string{path, strings.Join(args, " ")}, " ")

	fmt.Println("tugas-akhir-scripts: ", scripts)

	registerJobDefinitionOutput, err := c.registerJobDefinition(batchSvc, stepid, imageName, scripts, volumes, mountPoints, environments)

	if err != nil {
		return nil, nil, nil, nil, fmt.Errorf("register job definition: %w", err)
	}

	return registerJobDefinitionOutput, canarys, fileSystemIds, mountTargetIds, nil
}

func (c *executorAWSBatch) transferTaskBuildLog(
	plan atc.TaskPlan,
	delegate exec.BuildStepDelegate,
	procOuts [][]byte,
	procErrs [][]byte,
) ([][]byte, [][]byte, error) {
	delegate.Stdout().Write(procOuts[0])
	delegate.Stderr().Write(procErrs[0])

	return procOuts[1:], procErrs[1:], nil
}
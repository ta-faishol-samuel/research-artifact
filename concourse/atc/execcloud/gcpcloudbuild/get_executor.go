package gcpcloudbuild

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"strings"

	cloudbuildpb "cloud.google.com/go/cloudbuild/apiv1/v2/cloudbuildpb"

	"github.com/concourse/concourse/atc"
	"github.com/concourse/concourse/atc/exec"
)

func (c *executorGCPCloudBuild) convertGetStep(plan atc.GetPlan) ([]*cloudbuildpb.BuildStep, []int, error) {
	var steps []*cloudbuildpb.BuildStep
	var canarys []int
	if plan.Type == "registry-image" {
		return steps, canarys, nil
	} 

	planSource, err := json.Marshal(map[string]interface{}{"source": plan.Source, "version": plan.Version, "params": plan.Params})
	if err != nil {
		return steps, canarys, fmt.Errorf("marshal get plan source: %w", err)
	}
	
	canary := rand.Int() % 1000
	canarys = append(canarys, canary)
	
	scripts := []string{
		fmt.Sprintf("echo '%s' > /tmp/in-request", string(planSource[:])),
		fmt.Sprintf("/opt/resource/in /tmp/build/%d/%s < /tmp/in-request > /tmp/in-result", canary, plan.Name),
		fmt.Sprintf("echo 'BGN-%d'", canary),
		"cat /tmp/in-result",
		fmt.Sprintf("echo 'END-%d'", canary),
	}
	
	volumes := []*cloudbuildpb.Volume{&cloudbuildpb.Volume{
		Name: plan.Name,
		Path: fmt.Sprintf("/tmp/build/%d/%s", canary, plan.Name),
	}}

	steps = append(steps, &cloudbuildpb.BuildStep{
		Name: atc.GetCheckStepBaseImage(plan.Type),
		Script: strings.Join(scripts, "\n"),
		Volumes: volumes,
	})

	return steps, canarys, nil
}

func (c *executorGCPCloudBuild) transferGetBuildLog(
	plan atc.GetPlan,
	delegate exec.BuildStepDelegate,
	procOuts [][]byte,
	procErrs [][]byte,
) ([][]byte, [][]byte, error) {
	if plan.Type == "registry-image" {
		return procOuts, procErrs, nil
	} 
	delegate.Stdout().Write(procOuts[0])
	delegate.Stderr().Write(procErrs[0])

	return procOuts[1:], procErrs[1:], nil
}
package gcpcloudbuild

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"math/rand"
	"strings"

    cloudbuildpb "cloud.google.com/go/cloudbuild/apiv1/v2/cloudbuildpb"

	"github.com/concourse/concourse/atc"
)

func (c *executorGCPCloudBuild) ExecuteCheckStep(ctx context.Context, plan atc.CheckPlan, stderr io.Writer) ([]atc.Version, error) {
	steps, canary, err := c.convertCheckStep(plan)
	if err != nil {
		return nil, fmt.Errorf("convert check step: %w", err)
	}

	cbResult, err := c.runCloudBuild(ctx, steps)
	if err != nil {
		return nil, fmt.Errorf("run cloud build: %w", err)
	}

	procOuts, procErrs, err := c.getBuildLog(ctx, cbResult, []int{canary})
	if err != nil {
		return nil, fmt.Errorf("get cloud build logs: %w", err)
	}
	for _, procErr := range procErrs {
		stderr.Write(procErr)
	}

	var versions []atc.Version
	err = json.Unmarshal(procOuts[0], &versions)
	if err != nil {
		return nil, fmt.Errorf("unmarshal process output: %w", err)
	}
	
	return versions, nil
}

func (c *executorGCPCloudBuild) convertCheckStep(plan atc.CheckPlan) ([]*cloudbuildpb.BuildStep, int, error) {
	var step []*cloudbuildpb.BuildStep

	planSource, err := json.Marshal(map[string]interface{}{"source": plan.Source})
	if err != nil {
		return step, -1, fmt.Errorf("marshal check plan source: %w", err)
	}
	canary := rand.Int() % 1000
	scripts := []string{
		fmt.Sprintf("echo '%s' > /tmp/check-request", string(planSource[:])),
		"/opt/resource/check < /tmp/check-request > /tmp/check-result",
		fmt.Sprintf("echo 'BGN-%d'", canary),
		"cat /tmp/check-result",
		fmt.Sprintf("echo 'END-%d'", canary),
	}

	step = append(step, &cloudbuildpb.BuildStep{
		Name: atc.GetCheckStepBaseImage(plan.Type),
		Script: strings.Join(scripts, "\n"),
	})
	return step, canary, nil
}

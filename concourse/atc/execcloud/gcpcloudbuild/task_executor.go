package gcpcloudbuild

import (
	"fmt"
	"math/rand"
	"path/filepath"
	"strconv"

	cloudbuildpb "cloud.google.com/go/cloudbuild/apiv1/v2/cloudbuildpb"

	"github.com/concourse/concourse/atc"
	"github.com/concourse/concourse/atc/exec"
)

func parseImageResourceSource(imageConfig atc.Source) string {
	imageName := imageConfig["repository"].(string)
	imageTag, exist := imageConfig["tag"].(string)
	if exist {
		imageName = fmt.Sprintf("%s:%s", imageName, imageTag)
	}
	return imageName
}

func (c *executorGCPCloudBuild) convertTaskStep(plan atc.TaskPlan, resources map[string]atc.GetPlan) ([]*cloudbuildpb.BuildStep, []int, error) {
	var canarys []int
	canary := rand.Int() % 1000
	canarys = append(canarys, canary)

	var imageConfig atc.Source
	if len(plan.ImageArtifactName) != 0 {
		getPlan := resources[plan.ImageArtifactName]
		imageConfig = getPlan.Source
	} else {
		imageConfig = plan.Config.ImageResource.Source
	}
	imageName := parseImageResourceSource(imageConfig)
	
	var buildEnv []string
	for k, v := range plan.Config.Params {
        buildEnv = append(buildEnv, fmt.Sprintf("%s=%s", k, v))
    }

	buildWorkDir := filepath.Join("/tmp/build", strconv.Itoa(canary))
	
	var volumes []*cloudbuildpb.Volume
	for _, input := range plan.Config.Inputs {
		volumes = append(volumes, &cloudbuildpb.Volume{
			Name: input.Name,
			Path: artifactPath(buildWorkDir, input.Name, input.Path),
		})
	}
	for _, output := range plan.Config.Outputs {
		volumes = append(volumes, &cloudbuildpb.Volume{
			Name: output.Name,
			Path: artifactPath(buildWorkDir, output.Name, output.Path),
		})
	}
	
	steps := []*cloudbuildpb.BuildStep{&cloudbuildpb.BuildStep{
		Name: imageName,
		Env: buildEnv,
		Volumes: volumes,
		Entrypoint: plan.Config.Run.Path,
		Args: plan.Config.Run.Args,
		Dir: resolvePath(buildWorkDir, plan.Config.Run.Dir),
	}}

	return steps, canarys, nil
}

func (c *executorGCPCloudBuild) transferTaskBuildLog(
	plan atc.TaskPlan,
	delegate exec.BuildStepDelegate,
	procOuts [][]byte,
	procErrs [][]byte,
) ([][]byte, [][]byte, error) {
	delegate.Stdout().Write(procOuts[0])
	delegate.Stderr().Write(procErrs[0])

	return procOuts[1:], procErrs[1:], nil
}
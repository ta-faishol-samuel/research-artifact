package gcpcloudbuild

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
    "net/url"
	"os"
	"path/filepath"
	"time"
	
    cloudbuild "cloud.google.com/go/cloudbuild/apiv1/v2"
    cloudbuildpb "cloud.google.com/go/cloudbuild/apiv1/v2/cloudbuildpb"
	storage "cloud.google.com/go/storage"

	"github.com/concourse/flag"
    "google.golang.org/api/option"
	"github.com/concourse/concourse/atc"
	"github.com/concourse/concourse/atc/db"
	"github.com/concourse/concourse/atc/exec"
)

type executorGCPCloudBuild struct {
	cred		flag.File
	clientOpt	option.ClientOption
	projectId	string
}

func NewExecutorGCPCloudBuild(
	cred flag.File,
) exec.CloudExecutor {
	return &executorGCPCloudBuild{
		cred: cred,
		clientOpt: option.WithCredentialsFile(cred.Path()),
		projectId: "",
	}
}

func (c *executorGCPCloudBuild) getProjectId() string {
	if len(c.projectId) == 0 {
		credContent, _ := os.ReadFile(c.cred.Path())
		var data map[string]string
		err := json.Unmarshal(credContent, &data)
		if err != nil {
			return ""
		}
		c.projectId = data["project_id"]
	}

	return c.projectId
}

func (c *executorGCPCloudBuild) newCloudBuildClient(ctx context.Context) (*cloudbuild.Client, error) {
    return cloudbuild.NewClient(ctx, c.clientOpt)
}

func (c *executorGCPCloudBuild) newCloudBuildRequest(steps []*cloudbuildpb.BuildStep) *cloudbuildpb.CreateBuildRequest {
	projectId := c.getProjectId()
	return &cloudbuildpb.CreateBuildRequest{
        Parent: fmt.Sprintf("projects/%s/locations/global", projectId),
        ProjectId: projectId,
        Build: &cloudbuildpb.Build{
            Steps: steps,
            Options: &cloudbuildpb.BuildOptions{
                Logging: cloudbuildpb.BuildOptions_LEGACY,
            },
			Tags: []string{"cloudbuild-storage"},
        },
    }
}

func (c *executorGCPCloudBuild) runCloudBuild(ctx context.Context, steps []*cloudbuildpb.BuildStep) (*cloudbuildpb.Build, error) {	
	cbClient, err := c.newCloudBuildClient(ctx)
	if err != nil {
		return nil, fmt.Errorf("create cloud build client: %w", err)
	}
	defer cbClient.Close()

	cbRequest := c.newCloudBuildRequest(steps)

	cbBuild, err := cbClient.CreateBuild(ctx, cbRequest)
    if err != nil {
        return nil, fmt.Errorf("create cloud build: %w", err)
    }
	buildMetadata, err := cbBuild.Metadata()
	if err != nil {
		return nil, fmt.Errorf("fetch metadata: %w", err)
	}

	msgFactory := db.NewClientCloudMessageFactory()
	msgFactory.Create(atc.CloudMessage{
		Label: buildMetadata.GetBuild().GetId(),
		Message: "pending",
	})

	var (
		initDelay int = 3
		currentDelay int = 3
		maxDelay int = 60
	)
	for {
		msg, err := msgFactory.FindByLabel(buildMetadata.GetBuild().GetId())
		if err != nil {
			return nil, fmt.Errorf("refresh state cloud build: %w", err)
		}

		if msg[0].Message() != "done" {
			time.Sleep(time.Duration(currentDelay) * time.Second)
			currentDelay = 2 * currentDelay
			if currentDelay > maxDelay {
				currentDelay = initDelay
			}
		} else {
			break
		}
	}
	
	cbResult, err := cbBuild.Poll(ctx)
	if err != nil {
		return nil, fmt.Errorf("poll build: %w", err)
	}

	return cbResult, nil
}

func (c *executorGCPCloudBuild) newStorageClient(ctx context.Context) (*storage.Client, error) {
	return storage.NewClient(ctx, c.clientOpt)
}

func (c *executorGCPCloudBuild) getBuildLog(ctx context.Context, build *cloudbuildpb.Build, canarys []int) ([][]byte, [][]byte, error) {
	logFilename := fmt.Sprintf("log-%s.txt", build.GetId())
	logBucketUrl, err := url.Parse(build.GetLogsBucket())
	if err != nil {
		return nil, nil, fmt.Errorf("parse bucket url: %w", err)
	}
	
	storageClient, err := c.newStorageClient(ctx)
	if err != nil {
		return nil, nil, fmt.Errorf("create storage client: %w", err)
	}
	defer storageClient.Close()

	fileObj := storageClient.Bucket(logBucketUrl.Host).Object(logFilename)
	fileReader, err := fileObj.NewReader(ctx)
    if err != nil {
		return nil, nil, fmt.Errorf("create storage reader: %w", err)
    }
	defer fileReader.Close()

	logOut, err := io.ReadAll(fileReader)
	if err != nil {
		return nil, nil, fmt.Errorf("read build log output: %w", err)
    }
	resStdout, resStderr := c.parseLogOutput(logOut, canarys)
	return resStdout, resStderr, nil
}

func (c *executorGCPCloudBuild) parseLogOutput(log []byte, canarys []int) ([][]byte, [][]byte) {
	var resStdout [][]byte
	var resStderr [][]byte

	lenCanarys := len(canarys)

	signStr := "\nBUILD\n"
	if lenCanarys > 1 {
		signStr = "Starting Step #0"
	}
	posStr := bytes.Index(log, []byte(signStr)) + len(signStr)
	
	for i, canary := range canarys {
		var (
			nowOut []byte
			nowErr []byte
		)

		log = log[posStr:]

		// Get stderr
		endErr := fmt.Sprintf("BGN-%d", canary)
		posEnd := bytes.Index(log, []byte(endErr))

		if posEnd == -1 {
			// Stdout is empty
			endErr = "PUSH"
			if i != len(canarys) - 1 {
				endErr = fmt.Sprintf("Starting Step #%d", i + 1)
			}
			posEnd = bytes.Index(log, []byte(endErr))

			nowErr = log[:posEnd - 1]
			nowOut = []byte{}
		} else {
			// Stdout is not empty
			nowErr = log[:posEnd - 1]
			log = log[posEnd+len(endErr):]
			
			// Get stdout
			signEnd := fmt.Sprintf("END-%d", canary)
			posEnd = bytes.Index(log, []byte(signEnd))

			nowOut = log[:posEnd - 1]
		}

		ptrRepl := fmt.Sprintf("Step #%d: ", i)
		nowErr = bytes.ReplaceAll(nowErr, []byte(ptrRepl), []byte(""))
		nowOut = bytes.ReplaceAll(nowOut, []byte(ptrRepl), []byte(""))

		resStderr = append(resStderr, nowErr)
		resStdout = append(resStdout, nowOut)
		log = log[posEnd:]

		signStr = fmt.Sprintf("Starting Step #%d", i + 1)
		posStr = bytes.Index(log, []byte(signStr)) + len(signStr)
	}
	
	return resStdout, resStderr
}

func artifactPath(workingDir string, name string, path string) string {
	subdir := path
	if path == "" {
		subdir = name
	}

	return resolvePath(workingDir, subdir)
}

func resolvePath(workingDir string, path string) string {
	if filepath.IsAbs(path) {
		return path
	}
	return filepath.Join(workingDir, path)
}
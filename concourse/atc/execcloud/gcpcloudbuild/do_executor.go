package gcpcloudbuild

import (
	"context"
	"fmt"
	// "os"
	// "encoding/json"

    cloudbuildpb "cloud.google.com/go/cloudbuild/apiv1/v2/cloudbuildpb"
	
	"code.cloudfoundry.org/lager"
	"code.cloudfoundry.org/lager/lagerctx"
	"github.com/concourse/concourse/atc"
	"github.com/concourse/concourse/atc/exec"
)

func (c *executorGCPCloudBuild) ExecuteDoStep(ctx context.Context, plan atc.DoPlan, planDelegate []exec.BuildStepDelegate) (bool, error) {
	steps, canarys, err := c.convertDoStep(plan)
	if err != nil {
		return false, fmt.Errorf("convert do step: %w", err)
	}
	
	cbResult, err := c.runCloudBuild(ctx, steps)
	if err != nil {
		return false, fmt.Errorf("run do cloud build: %w", err)
	}

	procOuts, procErrs, err := c.getBuildLog(ctx, cbResult, canarys)
	if err != nil {
		return false, fmt.Errorf("get cloud build logs: %w", err)
	}
	
	err = c.transferDoBuildLog(ctx, plan, planDelegate, procOuts, procErrs)
	if err != nil {
		return false, fmt.Errorf("get cloud build logs: %w", err)
	}

	return true, nil
} 

func (c *executorGCPCloudBuild) convertDoStep(plan atc.DoPlan) ([]*cloudbuildpb.BuildStep, []int, error) {
	var buildSteps []*cloudbuildpb.BuildStep
	var canarys []int
	resources := make(map[string]atc.GetPlan)

	for _, innerPlan := range plan {
		buildStep, canary, err := c.buildConvertStep(innerPlan, resources)
		if err != nil {
			return buildSteps, canarys, fmt.Errorf("convert innerPlan: %w", err)
		}
		buildSteps = append(buildSteps, buildStep...)
		canarys = append(canarys, canary...)
	}

	return buildSteps, canarys, nil
}

func (c *executorGCPCloudBuild) buildConvertStep(plan atc.Plan, resources map[string]atc.GetPlan) ([]*cloudbuildpb.BuildStep, []int, error) {
	if plan.Get != nil {
		resources[plan.Get.Name] = *plan.Get
		return c.convertGetStep(*plan.Get)
	}
	if plan.Task != nil {
		return c.convertTaskStep(*plan.Task, resources)
	}
	return []*cloudbuildpb.BuildStep{}, []int{}, fmt.Errorf("step is not support")
}

func (c *executorGCPCloudBuild) transferDoBuildLog(
	ctx context.Context,
	plan atc.DoPlan,
	planDelegate []exec.BuildStepDelegate,
	procOuts [][]byte,
	procErrs [][]byte,
) error {
	var err error
	for i, innerPlan := range plan {
		delegate := planDelegate[i]

		logger := lagerctx.FromContext(ctx)
		logger = logger.Session("do-inner-step", lager.Data{
			"plan-id": innerPlan.ID,
		})
		delegate.Initializing(logger)
		delegate.Starting(logger)

		procOuts, procErrs, err = c.extractPlanBuildLog(innerPlan, delegate, procOuts, procErrs)
		if err != nil {
			return err
		}
		delegate.Finished(logger, true)
	}
	return nil
}

func (c *executorGCPCloudBuild) extractPlanBuildLog(
	plan atc.Plan,
	delegate exec.BuildStepDelegate,
	procOuts [][]byte,
	procErrs [][]byte,
) ([][]byte, [][]byte, error) {
	if plan.Get != nil {
		return c.transferGetBuildLog(*plan.Get, delegate, procOuts, procErrs)
	}
	if plan.Task != nil {
		return c.transferTaskBuildLog(*plan.Task, delegate, procOuts, procErrs)
	}
	return procOuts, procErrs, fmt.Errorf("step is not support")
}
package execcloud

import (
	"context"
	"time"

	"code.cloudfoundry.org/lager"
	"code.cloudfoundry.org/lager/lagerctx"
	"github.com/concourse/concourse/atc"
	"github.com/concourse/concourse/atc/exec"
)

type doStep struct {
	planID 					atc.PlanID
	plan 					atc.DoPlan
	metadata 				exec.StepMetadata
	delegateFactory			exec.BuildStepDelegateFactory
	executor				exec.CloudExecutor
	planDelegateFactory		[]exec.BuildStepDelegateFactory
}

func NewDoStep(
	planID atc.PlanID,
	plan atc.DoPlan,
	metadata exec.StepMetadata,
	delegateFactory exec.BuildStepDelegateFactory,
	executor exec.CloudExecutor,
	planDelegateFactory []exec.BuildStepDelegateFactory,
) exec.Step {
	return &doStep{
		planID: planID,
		plan: plan,
		metadata: metadata,
		delegateFactory: delegateFactory,
		executor: executor,
		planDelegateFactory: planDelegateFactory,
	}
}

func (step *doStep) Run(ctx context.Context, state exec.RunState) (bool, error) {	
	delegate := step.delegateFactory.BuildStepDelegate(state)
	
	logger := lagerctx.FromContext(ctx)
	logger = logger.Session("do-step", lager.Data{
		"plan-id": step.planID,
	})
	delegate.Initializing(logger)
	
	var planDelegate []exec.BuildStepDelegate
	for _, fact := range step.planDelegateFactory {
		planDelegate = append(planDelegate, fact.BuildStepDelegate(state))
	}

	// Waiting for running slots
	currentTime := 1
	for !atc.IsCloudMaxRunReached() {
		time.Sleep(time.Duration(currentTime) * time.Second)
		currentTime = currentTime * 2
		if currentTime > 60 {currentTime = 1}
	}
	
	delegate.Starting(logger)
	atc.CloudNumberRunInc()
	
	ok, err := step.executor.ExecuteDoStep(ctx, step.plan, planDelegate)
	atc.CloudNumberRunDec()
	
	delegate.Finished(logger, ok)
	return ok, err
}

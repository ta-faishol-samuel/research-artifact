package execcloud

import (
	"context"
	"fmt"
	"time"

	"code.cloudfoundry.org/lager"
	"code.cloudfoundry.org/lager/lagerctx"
	"github.com/concourse/concourse/atc"
	"github.com/concourse/concourse/atc/creds"
	"github.com/concourse/concourse/atc/db"
	"github.com/concourse/concourse/atc/exec"
	"github.com/concourse/concourse/atc/metric"
	"github.com/concourse/concourse/atc/runtime"
)

type checkStep struct {
	planID 					atc.PlanID
	plan 					atc.CheckPlan
	metadata 				exec.StepMetadata
	resourceConfigFactory 	db.ResourceConfigFactory
	delegateFactory			exec.CheckDelegateFactory
	defaultCheckTimeout 	time.Duration
	executor				exec.CloudExecutor
}

func NewCheckStep(
	planID atc.PlanID,
	plan atc.CheckPlan,
	metadata exec.StepMetadata,
	resourceConfigFactory db.ResourceConfigFactory,
	delegateFactory exec.CheckDelegateFactory,
	defaultCheckTimeout time.Duration,
	executor exec.CloudExecutor,
) exec.Step {
	return &checkStep{
		planID: planID,
		plan: plan,
		metadata: metadata,
		resourceConfigFactory: resourceConfigFactory,
		delegateFactory: delegateFactory,
		defaultCheckTimeout: defaultCheckTimeout,
		executor: executor,
	}
}

func (step *checkStep) Run(ctx context.Context, state exec.RunState) (bool, error) {
	delegate := step.delegateFactory.CheckDelegate(state)
	ok, err := step.run(ctx, state, delegate)
	return ok, err
}

func (step *checkStep) run(ctx context.Context, state exec.RunState, delegate exec.CheckDelegate) (bool, error) {
	logger := lagerctx.FromContext(ctx)
	logger = logger.Session("check-step", lager.Data{
		"step-name": step.plan.Name,
	})
	
	delegate.Initializing(logger)

	source, err := creds.NewSource(state, step.plan.Source).Evaluate()
	
	// Hacks to create different resource config for each job on each pipeline
	source["jobId"] = step.metadata.JobID
	source["pipelineID"] = step.metadata.PipelineID
	if err != nil {
		return false, fmt.Errorf("resource config creds evaluation: %w", err)
	}

	var imageSpec runtime.ImageSpec
	var imageResourceCache db.ResourceCache
	// Assumption: Fetching image for taks step is ignored
	imageSpec.ResourceType = step.plan.TypeImage.BaseType
	
	resourceConfig, err := step.resourceConfigFactory.FindOrCreateResourceConfig(step.plan.Type, source, imageResourceCache)
	if err != nil {
		return false, fmt.Errorf("create resource config: %w", err)
	}
	scope, err := delegate.FindOrCreateScope(resourceConfig)
	if err != nil {
		return false, fmt.Errorf("create resource config scope: %w", err)
	}
	err = delegate.PointToCheckedConfig(scope)
	if err != nil {
		return false, fmt.Errorf("update resource config scope: %w", err)
	}

	lock, run, err := delegate.WaitToRun(ctx, scope)
	if err != nil {
		return false, fmt.Errorf("wait: %w", err)
	}
	logger.Debug("after-wait-to-run", lager.Data{"run": run, "scope": scope.ID()})

	if run {
		defer func() {
			err := lock.Release()
			if err != nil {
				logger.Error("failed-to-release-lock", err)
			}
		}()

		fromVersion := step.plan.FromVersion
		if fromVersion == nil {
			latestVersion, found, err := scope.LatestVersion()
			if err != nil {
				return false, fmt.Errorf("get latest version: %w", err)
			}

			if found {
				fromVersion = atc.Version(latestVersion.Version())
			}
		}

		metric.Metrics.ChecksStarted.Inc()

		_, buildId, err := delegate.UpdateScopeLastCheckStartTime(scope, !step.plan.IsResourceCheck())
		if err != nil {
			return false, fmt.Errorf("update check start time: %w", err)
		}

		if buildId != 0 {
			// Update build id in logger as in-memory build's id is only generated when starts to run check.
			logger = logger.WithData(lager.Data{"build": buildId})
			ctx = lagerctx.NewContext(ctx, logger)
		}

		// Waiting for running slots
		currentTime := 1
		for !atc.IsCloudMaxRunReached() {
			time.Sleep(time.Duration(currentTime) * time.Second)
			currentTime = currentTime * 2
			if currentTime > 60 {currentTime = 1}
		}

		delegate.Starting(logger)
		atc.CloudNumberRunInc()
		
		versions, err := step.executor.ExecuteCheckStep(ctx, step.plan, delegate.Stderr())
		atc.CloudNumberRunDec()

		if err != nil {
			metric.Metrics.ChecksFinishedWithError.Inc()

			return false, fmt.Errorf("cloud execute check step: %w", err)
		}

		metric.Metrics.ChecksFinishedWithSuccess.Inc()

		err = scope.SaveVersions(db.NewSpanContext(ctx), versions)
		if err != nil {
			return false, fmt.Errorf("save versions: %w", err)
		}

		if len(versions) > 0 {
			state.StoreResult(step.planID, versions[len(versions)-1])
		}

		_, err = delegate.UpdateScopeLastCheckEndTime(scope, true)
		if err != nil {
			return false, fmt.Errorf("update check end time: %w", err)
		}
	} else {
		latestVersion, found, err := scope.LatestVersion()
		if err != nil {
			return false, fmt.Errorf("get latest version: %w", err)
		}

		if found {
			state.StoreResult(step.planID, atc.Version(latestVersion.Version()))
		}
	}
	
	delegate.Finished(logger, true)

	return true, nil
}
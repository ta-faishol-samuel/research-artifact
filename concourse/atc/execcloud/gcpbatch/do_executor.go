package gcpbatch

import (
	"context"
	"fmt"

	batchpb "cloud.google.com/go/batch/apiv1/batchpb"
	
	"code.cloudfoundry.org/lager"
	"code.cloudfoundry.org/lager/lagerctx"
	"github.com/concourse/concourse/atc"
	"github.com/concourse/concourse/atc/exec"
	"github.com/google/uuid"
)

func (c *executorGCPBatch) ExecuteDoStep(ctx context.Context, plan atc.DoPlan, planDelegate []exec.BuildStepDelegate) (bool, error) {
	jobid := uuid.NewString()
	runnables, stepStt, err := convertDoStep(jobid, plan) 
	if err != nil {
		return false, fmt.Errorf("convert do step: %w", err)
	}
	
	batchJob, err := c.RunBatch(ctx, jobid, runnables)
	if err != nil {
		return false, fmt.Errorf("send batch job: %w", err)
	}
	
	stt, err := c.traceDoStep(ctx, plan, planDelegate, batchJob, stepStt)
	if err != nil {
		return false, err
	}

	return stt, nil
} 

func convertDoStep(jobid string, plan atc.DoPlan) ([]*batchpb.Runnable, map[string]bool, error) {
	runnables := []*batchpb.Runnable{}
	stepStt := make(map[string]bool)
	resources := make(map[string]interface{})

	for i, innerPlan := range plan {
		runnable, _, err := planToRunnable(jobid, innerPlan, resources)
		if err != nil {
			return nil, nil, fmt.Errorf("failed convert innerplan %d: %w", i, err)
		}
		if runnable == nil {
			continue
		}

		stepStt[innerPlan.ID.String()] = true
		runnables = append(runnables, runnable)
	}

	return runnables, stepStt, nil
}

func planToRunnable(jobid string, plan atc.Plan, resources map[string]interface{}) (*batchpb.Runnable, string, error) {
	if plan.Get != nil {
		return convertGetStep(jobid, *plan.Get, resources)
	}
	if plan.Task != nil {
		return convertTaskStep(jobid, *plan.Task, resources)
	}
	return nil, "", fmt.Errorf("step is not support")
}

func (c *executorGCPBatch) traceDoStep(
	ctx context.Context,
	plan atc.DoPlan,
	planDelegate []exec.BuildStepDelegate,
	job *batchpb.Job, 
	stepStt map[string]bool,
) (bool, error) {
	var err error
	currentTime := job.GetCreateTime().AsTime()
	status := true
	cnt := 0

	for i, innerPlan := range plan {
		delegate := planDelegate[i]
		
		logger := lagerctx.FromContext(ctx)
		logger = logger.Session("do-inner-step", lager.Data{
			"plan-id": innerPlan.ID,
		})

		delegate.Initializing(logger)
		delegate.Starting(logger)

		if _, ok := stepStt[innerPlan.ID.String()]; ok {			
			status, currentTime, err = c.traceRunnable(ctx, job, currentTime, cnt, delegate.Stdout(), delegate.Stderr())
			cnt += 1
			if err != nil {
				return false, err
			}
		}

		delegate.Finished(logger, status)		
	}

	return status, nil
}
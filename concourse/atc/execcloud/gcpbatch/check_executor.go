package gcpbatch

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"

	batchpb "cloud.google.com/go/batch/apiv1/batchpb"
	"github.com/concourse/concourse/atc"
	"github.com/google/uuid"
)

func (c *executorGCPBatch) ExecuteCheckStep(ctx context.Context, plan atc.CheckPlan, stderr io.Writer) ([]atc.Version, error) {
	stepid := uuid.NewString()
	runnables, err := convertCheckStep(stepid, plan)
	if err != nil {
		return nil, fmt.Errorf("convert check step: %w", err)
	}

	batchJob, err := c.RunBatch(ctx, stepid, runnables)
	if err != nil {
		return nil, fmt.Errorf("send batch job: %w", err)
	}

	stdout := bytes.NewBufferString("")
	stt, _, err := c.traceRunnable(ctx, batchJob, batchJob.GetCreateTime().AsTime(), 0, stdout, stderr)
	if (err != nil || !stt) {
		return nil, fmt.Errorf("execution failed: %w", err)
	}

	log.Println("tugas-akhir-check-executor-stdout: ", stdout.String())

	var versions []atc.Version
	err = json.Unmarshal(stdout.Bytes(), &versions)
	if err != nil {
		return nil, fmt.Errorf("unmarshal process output: %w", err)
	}

	return versions, nil
}

func convertCheckStep(id string, plan atc.CheckPlan) ([]*batchpb.Runnable, error) {
	planSource, err := json.Marshal(map[string]interface{}{"source": plan.Source})
	if err != nil {
		return nil, fmt.Errorf("marshal check plan source: %w", err)
	}
	
	baseImage := atc.GetCheckStepBaseImage(plan.Type)
	entrypoint := "/bin/sh"
	cmds := []string{
		"-c",
		fmt.Sprintf("echo '%s' | /opt/resource/check", string(planSource[:])),
	}

	runnables := []*batchpb.Runnable{
		createBatchRunnable(id, baseImage, entrypoint, cmds, []string{}, ""),
	}
	return runnables, nil
}
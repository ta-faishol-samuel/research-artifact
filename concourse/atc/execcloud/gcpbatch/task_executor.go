package gcpbatch

import (
	"fmt"

	batchpb "cloud.google.com/go/batch/apiv1/batchpb"

	"github.com/concourse/concourse/atc"
	"github.com/google/uuid"
)

func parseImageResourceSource(imageConfig atc.Source) string {
	imageName := imageConfig["repository"].(string)
	imageTag, exist := imageConfig["tag"].(string)
	if exist {
		imageName = fmt.Sprintf("%s:%s", imageName, imageTag)
	}
	return imageName
}

func convertTaskStep(jobid string, plan atc.TaskPlan, resources map[string]interface{}) (*batchpb.Runnable, string, error) {
	stepid := uuid.NewString()

	var imageConfig atc.Source
	if len(plan.ImageArtifactName) != 0 {
		getPlan := resources[plan.ImageArtifactName].(atc.GetPlan)
		imageConfig = getPlan.Source
	} else {
		imageConfig = plan.Config.ImageResource.Source
	}

	imageName := parseImageResourceSource(imageConfig)
	entrypoint := plan.Config.Run.Path
	cmd := plan.Config.Run.Args
	volumes := getStepVolume(stepid, plan.Config.Inputs, plan.Config.Outputs)
	runnable := createBatchRunnable(stepid, imageName, entrypoint, cmd, volumes, plan.Config.Run.Dir)

	for _, output := range plan.Config.Outputs {
		resources[output.Name] = output.Name
	}

	return runnable, stepid, nil
}
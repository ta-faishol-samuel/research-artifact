package gcpbatch

import (
	"encoding/json"
	"fmt"

	batchpb "cloud.google.com/go/batch/apiv1/batchpb"

	"github.com/concourse/concourse/atc"
	"github.com/google/uuid"
)

func convertGetStep(jobid string, plan atc.GetPlan, resources map[string]interface{}) (*batchpb.Runnable, string, error) {
	stepid := uuid.NewString()
	resources[plan.Name] = plan

	if plan.Type == "registry-image" {
		return nil, stepid, nil
	}

	planSource, err := json.Marshal(map[string]interface{}{"source": plan.Source, "version": plan.Version, "params": plan.Params})
	if err != nil {
		return nil, stepid, fmt.Errorf("marshal get plan source: %w", err)
	}

	image := atc.GetCheckStepBaseImage(plan.Type)
	entrypoint := "/bin/sh"
	outputs := []atc.TaskOutputConfig{atc.TaskOutputConfig{Name: plan.Name, Path: "/tmp/build/get"}}
	volumes := getStepVolume(stepid, []atc.TaskInputConfig{}, outputs)
	cmd := []string{
		"-c",
		fmt.Sprintf("echo '%s' | /opt/resource/in /tmp/build/get", planSource),
	}
	runnable := createBatchRunnable(stepid, image, entrypoint, cmd, volumes, "")
	return runnable, stepid, nil
}
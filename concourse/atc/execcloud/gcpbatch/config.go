package gcpbatch

import (
	"fmt"
	"path/filepath"

	"github.com/concourse/concourse/atc"
)

var (
	batchMaxRunDuration int64 = 3600
	batchMaxRetryCount int32 = 0
)

var (
	batchRegion string = "us-central1"
	batchMachineType string = "e2-medium"
	batchMachineCPU int64 = 1000
	batchMachineMemory int64 = 4096
)

func artifactPath(workingDir string, name string, path string) string {
	subdir := path
	if path == "" {
		subdir = name
	}

	return resolvePath(workingDir, subdir)
}

func resolvePath(workingDir string, path string) string {
	if filepath.IsAbs(path) {
		return path
	}
	return filepath.Join(workingDir, path)
}

func getStepOutputVolume(workdir string, output atc.TaskOutputConfig) (string) {
	return fmt.Sprintf("%s:%s:nocopy", output.Name, artifactPath(workdir, output.Name, output.Path))
}

func getStepInputVolume(workdir string, input atc.TaskInputConfig) (string) {
	return fmt.Sprintf("%s:%s", input.Name, artifactPath(workdir, input.Name, input.Path))
}

func getStepVolume(stepid string, inputs []atc.TaskInputConfig, outputs []atc.TaskOutputConfig) []string {
	workdir := getInitialWorkdir(stepid)
	volumes := []string{
		fmt.Sprintf("vol-%s:%s", stepid, getInitialWorkdir(stepid)),
	}

	for _, input := range inputs {
		volumes = append(volumes, getStepInputVolume(workdir, input))
	}
	for _, output := range outputs {
		volumes = append(volumes, getStepOutputVolume(workdir, output))
	}
	return volumes
}
func getInitialWorkdir(stepid string) string {
	return filepath.Join("/tmp/build", stepid)
}
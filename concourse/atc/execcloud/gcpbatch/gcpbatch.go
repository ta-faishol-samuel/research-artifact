package gcpbatch

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	logger "log"
	"os"
	"strings"
	"time"

	batch "cloud.google.com/go/batch/apiv1"
	batchpb "cloud.google.com/go/batch/apiv1/batchpb"
	durationpb "google.golang.org/protobuf/types/known/durationpb"

	"github.com/concourse/concourse/atc/db"
	"github.com/concourse/concourse/atc/exec"
	"github.com/concourse/flag"
	"google.golang.org/api/option"
)

type executorGCPBatch struct {
	cred		flag.File
	clientOpt	option.ClientOption
	projectId	string
}

func NewExecutorGCPBatch(
	cred flag.File,
) exec.CloudExecutor {
	return &executorGCPBatch{
		cred: cred,
		clientOpt: option.WithCredentialsFile(cred.Path()),
		projectId: "",
	}
}

func (c *executorGCPBatch) getProjectId() string {
	if len(c.projectId) == 0 {
		credContent, _ := os.ReadFile(c.cred.Path())
		var data map[string]string
		err := json.Unmarshal(credContent, &data)
		if err != nil {
			return ""
		}
		c.projectId = data["project_id"]
	}

	return c.projectId
}

func createBatchRunnable(stepid string, image string, entrypoint string, cmd []string, volumes []string, workdir string) *batchpb.Runnable {
	opts := fmt.Sprintf("--workdir %s", resolvePath(getInitialWorkdir(stepid), workdir)) 
	
	return &batchpb.Runnable{
		Executable: &batchpb.Runnable_Container_{
			Container: &batchpb.Runnable_Container{
				ImageUri: image,
				Entrypoint: entrypoint,
				Commands: cmd,
				Options: opts,
				Volumes: volumes,
			},
		},
	}
}

func (c *executorGCPBatch) RunBatch(ctx context.Context, jobid string, runnables []*batchpb.Runnable) (*batchpb.Job, error) {	
	batchClient, err := batch.NewClient(ctx, c.clientOpt)
	if err != nil {
		return nil, fmt.Errorf("create batch client: %w", err)
	}
	defer batchClient.Close()
	
	taskSpec := &batchpb.TaskSpec{
		Runnables: runnables,
		ComputeResource: &batchpb.ComputeResource{
			CpuMilli:  batchMachineCPU,
			MemoryMib: batchMachineMemory,
		},
		MaxRunDuration: &durationpb.Duration{ Seconds: batchMaxRunDuration},
		MaxRetryCount: batchMaxRetryCount,
	}
	
	batchJobSpec := &batchpb.Job{
		TaskGroups: []*batchpb.TaskGroup{{TaskCount: 1, TaskSpec: taskSpec}},
		AllocationPolicy: &batchpb.AllocationPolicy{
			Instances: []*batchpb.AllocationPolicy_InstancePolicyOrTemplate{{
				PolicyTemplate: &batchpb.AllocationPolicy_InstancePolicyOrTemplate_Policy{
					Policy: &batchpb.AllocationPolicy_InstancePolicy{
						MachineType: batchMachineType,
						BootDisk: &batchpb.AllocationPolicy_Disk{
							DataSource: &batchpb.AllocationPolicy_Disk_Image{
								Image: "batch-cos",	
							},
							Type: "pd-standard",
							SizeGb: 30,
						},
					},
				},
			}},
			Labels: map[string]string{
				"experiment-system": "batch-log",
			},
			Network: &batchpb.AllocationPolicy_NetworkPolicy{
				NetworkInterfaces: []*batchpb.AllocationPolicy_NetworkInterface{
					&batchpb.AllocationPolicy_NetworkInterface{
						Network: "global/networks/default",
						Subnetwork: fmt.Sprintf("regions/%s/subnetworks/default", batchRegion),
						NoExternalIpAddress: true,
					},
				},
			},
		},
		Priority: 99,
		LogsPolicy: &batchpb.LogsPolicy{
			Destination: batchpb.LogsPolicy_CLOUD_LOGGING,
		},
		Labels: map[string]string{
			"experiment-system": "batch-log",
		},
	}

	batchJobReq := &batchpb.CreateJobRequest{
		Parent: fmt.Sprintf("projects/%s/locations/%s", c.getProjectId(), batchRegion),
		Job: batchJobSpec,
	}
	
	batchJob, err := batchClient.CreateJob(ctx, batchJobReq)
    if err != nil {
        return nil, fmt.Errorf("schedule batch job: %w", err)
    }

	return batchJob, nil
}

func (c *executorGCPBatch) traceRunnable(
	ctx context.Context,
	job *batchpb.Job,
	timeStart time.Time,
	stepIdx int,
	stdout io.Writer,
	stderr io.Writer,
) (bool, time.Time, error) {
	batchClient, err := batch.NewClient(ctx, c.clientOpt)
	if err != nil {
		return false, time.Time{}, fmt.Errorf("create batch client: %w", err)
	}
	defer batchClient.Close()

	isDone := false
	msgFactory := db.NewClientCloudMessageFactory()
	currentJob := job

	for !isDone {
		msgIt, err := msgFactory.Fetch(job.GetUid())
		if err != nil {
			return false, time.Time{}, fmt.Errorf("fetch message from db: %w", err)
		}
		
		for !isDone {
			msg, isFound, err := msgIt.Consume()
			if err != nil {
				return false, time.Time{}, fmt.Errorf("iterate message from db: %w", err)
			}
			if !isFound {
				break
			}

			var log map[string]interface{}
			err = json.Unmarshal([]byte(msg.Message()), &log)
			if err != nil {
				return false, time.Time{}, fmt.Errorf("unmarshal log: %w", err)
			}

			logger.Println("tugas-akhir-check-executor-get-log: ", log)

			severity := int(log["severity"].(float64))
			payload := log["payload"].(string)

			if strings.Contains(payload, fmt.Sprintf("runnable %d exited", stepIdx)) {
				isDone = true
				break
			}
			
			if severity == 200 {
				stdout.Write([]byte(payload))
			} else {
				stderr.Write([]byte(payload))
			}
		}
		msgIt.Close()
	}
	
	currentJob, err = batchClient.GetJob(ctx, &batchpb.GetJobRequest{Name: currentJob.GetName()})
	if err != nil {
		return false, time.Time{}, fmt.Errorf("refresh job status: %w", err)
	}
	
	return currentJob.GetStatus().GetState() <= batchpb.JobStatus_SUCCEEDED, time.Now().UTC(), nil
}
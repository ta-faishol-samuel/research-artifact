package execcloud

import (
	"strings"

	"github.com/concourse/concourse/atc"
	"github.com/concourse/concourse/atc/exec"
	"github.com/concourse/concourse/atc/execcloud/awsbatch"
	"github.com/concourse/concourse/atc/execcloud/gcpbatch"
	"github.com/concourse/concourse/atc/execcloud/gcpcloudbuild"
)

func CloudExecutorFactory() exec.CloudExecutor {
	if strings.ToLower(atc.CloudEnginePlatform) == "gcpcloudbuild" {
		return gcpcloudbuild.NewExecutorGCPCloudBuild(atc.CloudEngineCred)
	} else if strings.ToLower(atc.CloudEnginePlatform) == "gcpbatch" {
		return gcpbatch.NewExecutorGCPBatch(atc.CloudEngineCred)
	} else if strings.ToLower(atc.CloudEnginePlatform) == "awsbatch" {
		return awsbatch.NewExecutorAWSBatch(atc.CloudEngineCred)
	}
	return nil
}
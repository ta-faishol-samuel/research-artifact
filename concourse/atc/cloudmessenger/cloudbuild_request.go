package cloudmessenger

import (
	"context"
	"fmt"
	
	cloudbuild "cloud.google.com/go/cloudbuild/apiv1/v2"
    cloudbuildpb "cloud.google.com/go/cloudbuild/apiv1/v2/cloudbuildpb"
	iterator "google.golang.org/api/iterator"
	
	"github.com/concourse/concourse/atc/db"
)

type cloudbuildReqEngine struct {
	msgFactory db.CloudMessageFactory
	projectId string

	ctx context.Context
	cbClient *cloudbuild.Client
}

func (e *cloudbuildReqEngine) Close() {
	e.cbClient.Close()
}

func (e *cloudbuildReqEngine) Init(msg *cloudMessenger) error {
	e.msgFactory = msg.cloudMessageFactory
	e.projectId = msg.getProjectId()

	ctx := context.Background()
	cbClient, err := cloudbuild.NewClient(ctx, msg.clientOpt)
	if err != nil {
		return err
	}
	e.ctx = ctx
	e.cbClient = cbClient

	return nil
}

func (e *cloudbuildReqEngine) Execute() error {
	listReq := &cloudbuildpb.ListBuildsRequest{
		Parent: fmt.Sprintf("projects/%s/locations/global", e.projectId),
		ProjectId: e.projectId,
		Filter: `status="QUEUED" OR status="WORKING"`,
	}
	
	buildPendingList, err := e.msgFactory.FindByMessage("pending")
	if err != nil {
		return fmt.Errorf("fetch pending build in db: %w", err)
	}
	
	mapPendingBuild := make(map[string]bool)
	for _, buildReq := range buildPendingList {
		mapPendingBuild[buildReq.Label()] = false
	}

	buildIt := e.cbClient.ListBuilds(e.ctx, listReq)
	for {
		build, err := buildIt.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return fmt.Errorf("failed read builds: %w", err)
		}
		mapPendingBuild[build.GetId()] = true
	}	
	
	finishBuild := make([]string, 0)
	for buildId, isPending := range mapPendingBuild {
		if !isPending {
			finishBuild = append(finishBuild, buildId)
		}
	}
	
	err = e.msgFactory.BulkUpdateMessage(finishBuild, "done")
	if err != nil {
		return fmt.Errorf("bulk update build status: %w", err)
	}

	return nil
}
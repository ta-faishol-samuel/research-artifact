package cloudmessenger

import (
	"context"

	batch "cloud.google.com/go/batch/apiv1"

	"github.com/concourse/concourse/atc/db"
)

type awsbatchReqEngine struct {
	msgFactory db.CloudMessageFactory
	projectId string

	ctx context.Context
	batchClient *batch.Client
}

func (e *awsbatchReqEngine) Close() {
	return
}

func (e *awsbatchReqEngine) Init(msg *cloudMessenger) error {
	return nil
}

func (e *awsbatchReqEngine) Execute() error {
	return nil
}
package cloudmessenger

import (
	"encoding/json"
	"fmt"
	"os"
	"strings"
	"time"

	"code.cloudfoundry.org/lager"

	"github.com/concourse/concourse/atc"
	"github.com/concourse/concourse/atc/db"
	"github.com/concourse/flag"
	"github.com/tedsuo/ifrit"
	"google.golang.org/api/option"
)

type CloudMessengerEngine interface {
	Init(*cloudMessenger) error
	Execute() error
	Close()
}

type cloudMessenger struct {
	logger		lager.Logger
	cred		flag.File
	clientOpt	option.ClientOption
	projectId	string
	cloudMessageFactory db.CloudMessageFactory
}

func NewCloudMessengerRunner(
	logger lager.Logger,
	cred flag.File,
	conn db.Conn,
) ifrit.Runner {
	return &cloudMessenger{
		logger: logger,
		cred: cred,
		clientOpt: option.WithCredentialsFile(cred.Path()),
		projectId: "",
		cloudMessageFactory: db.NewCloudMessageFactory(conn),
	}
}

func (c *cloudMessenger) getProjectId() string {
	if len(c.projectId) == 0 {
		credContent, _ := os.ReadFile(c.cred.Path())
		var data map[string]string
		err := json.Unmarshal(credContent, &data)
		if err != nil {
			return ""
		}
		c.projectId = data["project_id"]
	}

	return c.projectId
}

func (c *cloudMessenger) Run(signals <-chan os.Signal, ready chan<- struct{}) error {
	logger := c.logger.Session("cloud-messenger")
	
	logger.Info("start")
	defer logger.Info("finish")

	engine, err := engineFactory()
	if err != nil {
		logger.Error("failed-create-engine", err)
		return err
	}

	err = engine.Init(c)
	if err != nil {
		logger.Error("failed-init-engine", err)
		return err
	}
	defer engine.Close()

	close(ready)

	for {
		select {
		case <-signals:
			return nil
		default:
			logger.Debug("start-execute")
			err = engine.Execute()
			if err != nil {
				logger.Error("failed-execute-engine", err)
				return err
			}
			logger.Debug("done-execute")
			time.Sleep(15*time.Second)
		}
	}
}

func engineFactory() (CloudMessengerEngine, error) {
	if strings.ToLower(atc.CloudEnginePlatform) == "gcpbatch" {
		return &loggingBatchEngine{}, nil
	} else if strings.ToLower(atc.CloudEnginePlatform) == "gcpbatchstorage" {
		return &batchReqEngine{}, nil
	} else if strings.ToLower(atc.CloudEnginePlatform) == "gcpcloudbuild" || 
			strings.ToLower(atc.CloudEnginePlatform) == "gcpcloudbuildlog" {
		return &cloudbuildReqEngine{}, nil
	} else if strings.ToLower(atc.CloudEnginePlatform) == "awsbatch" {
		return &awsbatchReqEngine{}, nil
	}
	return nil, fmt.Errorf("cloud mode is not supported")
}
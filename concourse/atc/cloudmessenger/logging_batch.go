package cloudmessenger

import (
	"fmt"
	"context"
	"encoding/json"
	"time"

	iterator "google.golang.org/api/iterator"
	logadmin "cloud.google.com/go/logging/logadmin"

	"github.com/concourse/concourse/atc"
	"github.com/concourse/concourse/atc/db"
)

type loggingBatchEngine struct {
	msgFactory db.CloudMessageFactory

	ctx context.Context
	logClient *logadmin.Client
	baseFilter string
	lastCheck time.Time
}

func (l *loggingBatchEngine) Close() {
	l.logClient.Close()
}

func (l *loggingBatchEngine) Init(msg *cloudMessenger) error {
	l.msgFactory = msg.cloudMessageFactory
	l.ctx = context.Background()
	l.lastCheck = time.Now().UTC()
	
	logadminClient, err := logadmin.NewClient(l.ctx, msg.getProjectId(), msg.clientOpt)
	if err != nil {
		return fmt.Errorf("cloud messenger: create logadmin client: %w", err)
	}
	l.logClient = logadminClient

	batchTaskFilter := fmt.Sprintf(`logName="projects/%s/logs/batch_task_logs"`, msg.getProjectId())	
	batchAgentPtrn := `textPayload=~"task/job\S+ runnable \d+ exited"`
	batchAgentFilter := fmt.Sprintf(
		`logName="projects/%s/logs/batch_agent_logs" AND %s`,
		msg.getProjectId(), batchAgentPtrn,
	)
	l.baseFilter = fmt.Sprintf("(%s) OR (%s)", batchTaskFilter, batchAgentFilter)

	return nil
}

func (l *loggingBatchEngine) Execute() error {
	lastCheck, err := l.execute(l.ctx, l.logClient, l.baseFilter, l.lastCheck)
	l.lastCheck = lastCheck
	return err
}

func (l *loggingBatchEngine) execute(ctx context.Context, logadminClient *logadmin.Client, baseFilter string, lastCheck time.Time) (time.Time, error) {	
	timeNow := time.Now().UTC()
	logFilter := fmt.Sprintf(
		`receiveTimestamp >= "%s" AND receiveTimestamp < "%s" AND (%s)`,
		lastCheck.Format(time.RFC3339Nano),
		timeNow.Format(time.RFC3339Nano),
		baseFilter,
	)
	
	logIt := logadminClient.Entries(ctx, logadmin.Filter(logFilter))
	
	for {
		entry, err := logIt.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return time.Time{}, fmt.Errorf("failed read logs: %w", err)
		}
	
		data, err := json.Marshal(map[string]interface{}{
			"payload": entry.Payload,
			"severity": entry.Severity,
		})
		
		if err != nil {
			return time.Time{}, fmt.Errorf("failed convert log to json: %w", err)
		}

		l.msgFactory.Create(atc.CloudMessage{
			Label: entry.Labels["job_uid"],
			Message: string(data),
			MessageType: entry.LogName,
		})
	}	
	
	return timeNow, nil
}
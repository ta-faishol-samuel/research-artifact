package cloudmessenger

import (
	"context"
	"fmt"
	
	batch "cloud.google.com/go/batch/apiv1"
	batchpb "cloud.google.com/go/batch/apiv1/batchpb"
	iterator "google.golang.org/api/iterator"
	
	"github.com/concourse/concourse/atc/db"
)

type batchReqEngine struct {
	msgFactory db.CloudMessageFactory
	projectId string

	ctx context.Context
	batchClient *batch.Client
}

func (e *batchReqEngine) Close() {
	e.batchClient.Close()
}

func (e *batchReqEngine) Init(msg *cloudMessenger) error {
	e.msgFactory = msg.cloudMessageFactory
	e.projectId = msg.getProjectId()

	ctx := context.Background()
	batchClient, err := batch.NewClient(ctx, msg.clientOpt)
	if err != nil {
		return err
	}
	e.ctx = ctx
	e.batchClient = batchClient

	return nil
}

func (e *batchReqEngine) Execute() error {
	listReq := &batchpb.ListJobsRequest{
		Parent: fmt.Sprintf("projects/%s/locations/-", e.projectId),
		Filter: `status.state="QUEUED" OR status.state="SCHEDULED" OR status.state="RUNNING"`,
	}
	
	jobPendingList, err := e.msgFactory.FindByMessage("pending")
	if err != nil {
		return fmt.Errorf("fetch pending job in db: %w", err)
	}
	
	mapPendingJob := make(map[string]bool)
	for _, jobReq := range jobPendingList {
		mapPendingJob[jobReq.Label()] = false
	}

	jobIt := e.batchClient.ListJobs(e.ctx, listReq)
	for {
		job, err := jobIt.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return fmt.Errorf("read job: %w", err)
		}
		// fmt.Println(job)
		mapPendingJob[job.GetUid()] = true
	}	
	
	finishJob := make([]string, 0)
	for jobId, isPending := range mapPendingJob {
		if !isPending {
			finishJob = append(finishJob, jobId)
		}
	}
	
	err = e.msgFactory.BulkUpdateMessage(finishJob, "done")
	if err != nil {
		return fmt.Errorf("bulk update job status: %w", err)
	}

	return nil
}
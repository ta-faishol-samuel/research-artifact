package atc

import (
	"fmt"
	"time"
	"sync/atomic"

	"github.com/concourse/flag"
)

type CloudMessage struct {
	Label string 
	Message string
	MessageType string
}

var (
	CloudEnginePlatform	string
	CloudEngineCred		flag.File
	MaxNumberCloudRequest int = 50
	MaxNumberCloudRun int = 50
)

var (
	numberCloudRequest int = 0
	numberCloudRun int32 = 0
	lastTickCloudRequest time.Time = time.Now().UTC()
)

var CloudSupportedResourceTypes []string = []string{"git", "registry-image", "time"}

func IsCloudEngineEnable() bool {
	return len(CloudEnginePlatform) != 0
}

func GetCheckStepBaseImage(resourceType string) string {
	if resourceType == "git" {
		return "concourse/git-resource:1.14"
	} else if resourceType == "registry-image" {
		return "concourse/registry-image-resource:1.8"
	} else {
		return fmt.Sprintf("concourse/%s-resource", resourceType)
	}
}

func IsCloudMaxRequestReached() bool {
	return numberCloudRequest < MaxNumberCloudRequest
}

func CloudNumberRequestInc() {
	timeNow := time.Now().UTC()
	if timeNow.Sub(lastTickCloudRequest) > 1 * time.Minute {
		lastTickCloudRequest = timeNow;
		numberCloudRequest = 0;
	}
	numberCloudRequest = numberCloudRequest + 1
}

func IsCloudMaxRunReached() bool {
	cnt := atomic.LoadInt32(&numberCloudRun)
	return cnt < int32(MaxNumberCloudRun)
}

func CloudNumberRunInc() {
	atomic.AddInt32(&numberCloudRun, 1)
}

func CloudNumberRunDec() {
	atomic.AddInt32(&numberCloudRun, -1)
}
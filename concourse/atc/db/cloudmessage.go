package db

import (
	"database/sql"

	sq "github.com/Masterminds/squirrel"
	"github.com/concourse/concourse/atc"
)

var cloudmessagesQuery = psql.Select("m.id, m.label, m.message, m.msgtype").
	From("cloudmessages m")

var clientCloudMessageFactory CloudMessageFactory = nil

type CloudMessage interface {
	ID() int
	Label() string
	Message() string
	MessageType() string
}

type cloudMessage struct {
	id       int
	label    string
	message	 string
	msgType	 string
}

func (m *cloudMessage) ID() int 		{ return m.id }
func (m *cloudMessage) Label() string 	{ return m.label }
func (m *cloudMessage) Message() string { return m.message }
func (m *cloudMessage) MessageType() string { return m.msgType }

func scanCloudMessage(c *cloudMessage, row scannable) error {
	err := row.Scan(
		&c.id,
		&c.label,
		&c.message,
		&c.msgType,
	)
	if err != nil {
		return err
	}
	return nil
}

type CloudMessageIterator interface {
	Consume() (CloudMessage, bool, error)
	Close() error
}

type cloudMessageIterator struct {
	row *sql.Rows
	conn Conn
}

func (c *cloudMessageIterator) Consume() (CloudMessage, bool, error) {
	stt := c.row.Next()
	if !stt {
		return nil, false, nil
	}	

	obj := &cloudMessage{}
	err := scanCloudMessage(obj, c.row)
	if err != nil {
		return nil, false, err
	}

	_, err = psql.Delete("cloudmessages").
		Where(sq.Eq{"id": obj.ID()}).
		RunWith(c.conn).
		Exec()
	if err != nil {
		return nil, false, err
	}

	return obj, true, nil
}

func (c *cloudMessageIterator) Close() error {
	defer c.row.Close()
	
	tx, err := c.conn.Begin()
	if err != nil {
		return err
	}

	err = tx.Commit()
	if err != nil {
		return err
	}
	return nil
}

type CloudMessageFactory interface {
	BulkUpdateMessage([]string, string) (error) 
	Create(atc.CloudMessage) error
	Fetch(string) (CloudMessageIterator, error)
	FindByLabel(string) ([]CloudMessage, error)
	FindByMessage(string) ([]CloudMessage, error)
}

type cloudMessageFactory struct {
	conn Conn
}

func NewCloudMessageFactory(conn Conn) CloudMessageFactory {
	if clientCloudMessageFactory == nil {
		clientCloudMessageFactory = &cloudMessageFactory{
			conn: conn,
		}
	}
	return clientCloudMessageFactory
}

func NewClientCloudMessageFactory() CloudMessageFactory {
	return clientCloudMessageFactory
}

func (f *cloudMessageFactory) Create(msg atc.CloudMessage) error {
	tx, err := f.conn.Begin()
	if err != nil {
		return err
	}

	_, err = psql.Insert("cloudmessages").
		Columns("label", "message", "msgtype").
		Values(msg.Label, msg.Message, msg.MessageType).
		RunWith(f.conn).
		Exec()	
	
	if err != nil {
		return err
	}
	
	err = tx.Commit()
	if err != nil {
		return err
	}

	return nil
}

func (f *cloudMessageFactory) Fetch(label string) (CloudMessageIterator, error) {
	rows, err := cloudmessagesQuery.
		Where(sq.Eq{"m.label": label}).
		RunWith(f.conn).
		Query()
	if err != nil {
		return nil, err
	}

	return &cloudMessageIterator{
		row: rows,
		conn: f.conn,
	}, nil
}

func (f *cloudMessageFactory) FindByLabel(label string) ([]CloudMessage, error) {
	return f.find(sq.Eq{"m.label": label})
}

func (f *cloudMessageFactory) FindByMessage(msg string) ([]CloudMessage, error) {
	return f.find(sq.Eq{"m.message": msg})
}

func (f *cloudMessageFactory) find(pred interface{}) ([]CloudMessage, error) {
	rows, err := cloudmessagesQuery.
		Where(pred).
		RunWith(f.conn).
		Query()
	if err != nil {
		return nil, err
	}

	objs := make([]CloudMessage, 0)
	for rows.Next() {
		obj := &cloudMessage{}
		err = scanCloudMessage(obj, rows)
		if err != nil {
			return nil, err
		}
		objs = append(objs, obj)
	}
	return objs, nil
}

func (f *cloudMessageFactory) BulkUpdateMessage(labels []string, newMsg string) (error) {
	if len(labels) == 0 { return nil }
	
	tx, err := f.conn.Begin()
	if err != nil {
		return err
	}

	_, err = psql.Update("cloudmessages").
		Set("message", newMsg).
		Where(sq.Eq{
			"label": labels,
		}).
		RunWith(tx).
		Exec()
	if err != nil {
		return err
	}

	err = tx.Commit()
	if err != nil {
		return err
	}

	return nil
}
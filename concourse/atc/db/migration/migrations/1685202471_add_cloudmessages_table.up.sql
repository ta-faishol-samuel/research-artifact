CREATE TABLE "cloudmessages" (
    "id"        SERIAL PRIMARY KEY,
    "label"     VARCHAR(200) NOT NULL,
    "message"   TEXT,
    "msgtype"   VARCHAR(50)
);
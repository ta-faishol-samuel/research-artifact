package exec

import (
	"context"
	"io"

	"github.com/concourse/concourse/atc"
)

type CloudExecutor interface {
	ExecuteCheckStep(context.Context, atc.CheckPlan, io.Writer) ([]atc.Version, error)
	ExecuteDoStep(context.Context, atc.DoPlan, []BuildStepDelegate) (bool, error)
}

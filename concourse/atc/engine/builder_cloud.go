package engine

import (
	"errors"

	"github.com/concourse/concourse/atc"
	"github.com/concourse/concourse/atc/db"
	"github.com/concourse/concourse/atc/exec"
	"github.com/concourse/concourse/atc/execcloud"
)

const cloudSupportedSchema = "exec.v2"

type cloudStepperFactory stepperFactory

func (factory *cloudStepperFactory) StepperForBuild(build db.Build) (exec.Stepper, error) {
	if build.Schema() != cloudSupportedSchema {
		return nil, errors.New("schema not supported")
	}

	return func(plan atc.Plan) exec.Step {
		return factory.buildStep(build, plan)
	}, nil
}

func (factory *cloudStepperFactory) buildDelegateFactory(build db.Build, plan atc.Plan) DelegateFactory {
	return DelegateFactory{
		build:           build,
		plan:            plan,
		rateLimiter:     factory.rateLimiter,
		policyChecker:   factory.policyChecker,
		dbWorkerFactory: factory.dbWorkerFactory,
		lockFactory:     factory.lockFactory,
	}
}

func (factory *cloudStepperFactory) buildStep(build db.Build, plan atc.Plan) exec.Step {
	if plan.Check != nil {
		return factory.buildCheckStep(build, plan)
	}

	if plan.Do != nil {
		return factory.buildDoStep(build, plan)
	}
	return nil
}

func (factory *cloudStepperFactory) buildCheckStep(build db.Build, plan atc.Plan) exec.Step {
	stepMetadata := factory.stepMetadata(build, false)
	executor := execcloud.CloudExecutorFactory()
	return execcloud.NewCheckStep(
		plan.ID,
		*plan.Check,
		stepMetadata,
		factory.coreFactory.ResourceConfigFactory(),
		factory.buildDelegateFactory(build, plan),
		factory.coreFactory.DefaultCheckTimeout(),
		executor,
	)
}

func (factory *cloudStepperFactory) buildDoStep(build db.Build, plan atc.Plan) exec.Step {
	stepMetadata := factory.stepMetadata(build, false)
	executor := execcloud.CloudExecutorFactory()

	planDelegateFactory := []exec.BuildStepDelegateFactory{}
	for _, p := range *plan.Do {
		fact := factory.buildDelegateFactory(build, p)
		planDelegateFactory = append(planDelegateFactory, fact)
	}

	return execcloud.NewDoStep(
		plan.ID,
		*plan.Do,
		stepMetadata,
		factory.buildDelegateFactory(build, plan),
		executor,
		planDelegateFactory,
	)
}

func (factory *cloudStepperFactory) stepMetadata(
	build db.Build,
	exposeBuildCreatedBy bool,
) exec.StepMetadata {
	meta := exec.StepMetadata{
		BuildID:              build.ID(),
		BuildName:            build.Name(),
		TeamID:               build.TeamID(),
		TeamName:             build.TeamName(),
		JobID:                build.JobID(),
		JobName:              build.JobName(),
		PipelineID:           build.PipelineID(),
		PipelineName:         build.PipelineName(),
		PipelineInstanceVars: build.PipelineInstanceVars(),
		ExternalURL:          "",
	}
	if exposeBuildCreatedBy && build.CreatedBy() != nil {
		meta.CreatedBy = *build.CreatedBy()
	}
	return meta
}

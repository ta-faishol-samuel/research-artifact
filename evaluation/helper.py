from main import *
from random import randint, seed, choices
import datetime
import sys

USERNAME = "test"
PASSWORD = "test"

HOST = "localhost"
PORT = "8080"

MODELS = ["simple-go", "simple-go-b1", "simple-go-b2", "simple-go-b3", "simple-go-b4"]

def con01(team_name):
    print("Running constant 1...")
    run_simulation(models=MODELS, functype="constant", max_trigger=1,
        cycle_len=3, num_cycle=(60 // 3),
        delay_iter=3 * 60, delay_cycle=0, team_name=team_name)

def con02(team_name):
    print("Running constant 2...")
    run_simulation(models=MODELS, functype="constant", max_trigger=4,
        cycle_len=3, num_cycle=(60 // 3),
        delay_iter=3 * 60, delay_cycle=0, team_name=team_name)

def con03(team_name):
    print("Running constant 3...")
    run_simulation(models=MODELS, functype="constant", max_trigger=10,
        cycle_len=3, num_cycle=(60 // 3),
        delay_iter=3 * 60, delay_cycle=0, team_name=team_name)

def crv01(team_name):
    print("Running curve 1...")
    time_percycle = 30 + 3 * 4 * 2
    run_simulation(models=MODELS, functype="curve", max_trigger=10,
        cycle_len=4, num_cycle=12 * 60 // time_percycle,
        delay_iter=3 * 60, delay_cycle=30 * 60, team_name=team_name)

def crv02(team_name):
    print("Running curve 2...")
    time_percycle = 30 + 3 * 6 * 2
    run_simulation(models=MODELS, functype="curve", max_trigger=25,
        cycle_len=6, num_cycle=12 * 60 // time_percycle,
        delay_iter=3 * 60, delay_cycle=30 * 60, team_name=team_name)

def rnd01(team_name):
    time_delay = [5, 15, 30, 60, 75]
    print("Running random scenario...")

    seed(2023)
    start_time = datetime.datetime.now()

    models = MODELS
    total_job = 100
    populate_pipeline(team_name,models, total_job)
    
    iterasi = 0
    accum_job = 0

    for i in range(13):
        current_time = datetime.datetime.now()
        duration = current_time - start_time
        if duration.days > 2:
            break 

        num_job = randint(1, 25)
        if accum_job + num_job > total_job:
            num_job = total_job - accum_job

        trigger_job(team_name, accum_job, num_job, iterasi)
        accum_job += num_job
        
        if accum_job >= total_job:
            accum_job = 0
            iterasi = 0

        time.sleep(choices(time_delay)[0] * 60)

if __name__ == "__main__":
    scenario = sys.argv[1]
    server = sys.argv[2]
    if len(sys.argv) >= 4:
        HOST = sys.argv[3]
        PORT = sys.argv[4]
    
    url = f"http://{HOST}:{PORT}/"
    team_name = server
    fly_login(url, USERNAME, PASSWORD, team_name)
    globals()[scenario](team_name)
    

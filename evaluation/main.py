import sys
import argparse
import time
import os
import datetime

# t = time now
# n = max build job trigger
# k = cycle length
FUNC = {
    "constant": lambda t, n, k: n,
    "linpos": lambda t, n, k: int(((t % k) + 1) * n / k),
    "linneg": lambda t, n, k: int((k - (t % k)) * n / k),
    "curve": lambda t, n, k: int((t % k) * n / k) if (t % (2*k)) < k else int((k - (t % k)) * n / k),
}

PIPELINES_PATH = "pipelines"
FLY_PATH = "./fly"
TEAM_NAME = "simulator-team"

def get_total_job(functype, max_trigger, cycle_len):
    return sum([FUNC[functype](i, max_trigger, cycle_len) for i in range(cycle_len)])

def populate_pipeline(team_name, models, num):
    models_len = len(models)
    for i in range(num):
        model = models[i % models_len]
        yamlfile = os.path.join(PIPELINES_PATH, f"{model}.yml")
        os.system(f"{FLY_PATH} -t {team_name} set-pipeline -c {yamlfile} -p pipeline{i} -n")

        
def trigger_job(team_name, idxstr, num, t=0):
    print(datetime.datetime.now(), num, flush=True)
    for i in range(idxstr, idxstr+num):
        os.system(f"{FLY_PATH} -t {team_name} unpause-pipeline -p pipeline{i}")
        os.system(f"{FLY_PATH} -t {team_name} trigger-job -j pipeline{i}/build")

"""
    @param models       : array of pipelines models filename
    @param functype     : function to calculate the number of build job to be trigger
    @param max_trigger  : the maximum build job triggered for every trigger request
    @param cycle_len    : number of trigger request for one cycle
    @param num_cycle    : number of cycle
    @param delay_iter   : delay time between trigger job request (in second)
    @param delay_cycle  : delay time between cycle (in second)
"""
def run_simulation(team_name, models=["simple-go"], functype="constant", max_trigger=4, cycle_len=5, num_cycle=2, delay_iter=30, delay_cycle=30):
    if functype == "curve":
        cycle_len *= 2
    
    total_job = get_total_job(functype, max_trigger, cycle_len)
    populate_pipeline(team_name, models, total_job)

    for i in range(num_cycle):
        accum_job = 0
        for j in range(cycle_len):
            num_job = FUNC[functype](j, max_trigger, cycle_len)
            trigger_job(team_name, accum_job, num_job, i)
            accum_job += num_job

            time.sleep(delay_iter)
        time.sleep(delay_cycle)

def fly_login(url, username, password, team_name = None, team="main"):
    if team_name == None: team_name = TEAM_NAME
    os.system(f"{FLY_PATH} -t {team_name} login -c {url} -u {username} -p {password} -n {team}")

def main():
    fly_login("http://localhost:8080", "test", "test")
    
    # models = nama file config pipeline yang ada di folder pipelines
    run_simulation(TEAM_NAME, models=["simple-go"], max_trigger=5, cycle_len=2, num_cycle=1)

if __name__ == "__main__":
    main()